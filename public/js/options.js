//OPTIONS FUNCTION TABLE

var options_alarm_status_initial = [
    "5th Alarm",
    "4th Alarm",
    "3rd Alarm",
    "2nd Alarm",
    "1st Alarm",
    "Task Force Alpha",
    "Task Force Bravo",
    "Task Force Charlie",
    "Task Force Delta",
    "Task Force Echo",
    "General Alarm"
]; //END OF ALARM STATUS OPTIONS

var options_alarm_status = [
    "Fire Out",
    "Under Control",
    "5th Alarm",
    "4th Alarm",
    "3rd Alarm",
    "2nd Alarm",
    "1st Alarm",
    "Task Force Alpha",
    "Task Force Bravo",
    "Task Force Charlie",
    "Task Force Delta",
    "Task Force Echo",
    "General Alarm"
]; //END OF ALARM STATUS OPTIONS


var options_wind_directions = [
    "Please Select One!",
    "North",
    "North-North-East",
    "North East",
    "East-North-East",
    "East",
    "East-South-East",
    "South East",
    "South-South-East",
    "South",
    "South-South-West",
    "South West",
    "West-South-West",
    "West",
    "West-North-West",
    "North West",
    "North-North-West"
];// END OF WIND DIRECTIONS OPTIONS

var options_building_structure = [
    "Non-Residential",
    "Residential",
    "Commercial",
    "Educational",
    "Government",
    "Industrial",
    "Agricultural",
    "Religious",
    "Parking/Storage",
    "Transportaion",
    "Infrastructure",
    "Power Stations",
    "Others"
];// END OF BUILDING STRUCTURE OPTIONS

var options_fire_type = [
    "Ordinary, Combustible Materials",
    "Flammable, Liquid and Gas",
    "Electrical",
    "Cooking Oils and Fats",
    "Metal"
]; //END OF FIRE TYPE OPTIONS

var options_confirm_decline = [
    "Please Select One!",
    "Already Reported",
    "Out of Coverage Area",
    "False Information",
    "Incomplete Information",
    "Intentional Misleading",
    "Prank",
    "Others"
]; //END OF CONFIRM DECLINE OPTIONS
