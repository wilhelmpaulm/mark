// ALL METHODS DELCARATIONS AND FUNCTIONS


//SELECT DROPDOWN
function method_select_dropdown(value1, value2, value3) {
    // PARAMETERS (Selected Value, Array Value, ARRAY)
    for (var x = 0; value3.length > x; x++) {
        if ($("#" + value1).val() == value3[x]) {
            $("#" + value2).append("<option selected=''>" + value3[x] + "</option>");
        } else {
            $("#" + value2).append("<option>" + value3[x] + "</option>");
        }
    }
} //END OF FUNCTION

//SHOW AND HIDE
function method_show_hide (value1, value2, value3) {
    // PARAMETERS (Selected Value, Target Value, Response Format)        
   $(value1).on("change", function(){        
        if ($(value1).val() == value2) {
            $(value3).hide();
        }
        else{
            $(value3).show();
        }
    });
}

//SHOW AND HIDE (RADIO)
function method_show_hide_radio (value1, value2, value3) {
    // PARAMETERS (Selected Value, Target Value, Response Format)        
   $("input:radio[name="+value1+"]").on("change", function(){        
        if ($(this).filter(":checked").val() == value2) {
            $(value3).hide();
        }
        else{
            $(value3).show();
        }
    });
}