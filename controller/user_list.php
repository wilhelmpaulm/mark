
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">List of ALL Users</h1>
            <!-- WE NEED A DATE SELECTOR FEATURE-->
            <div class="row">
                <div class="col-md-6">            
                    <table class="table table-bordered table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>Last Name</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Birthday</th>
                                <th>Contact Number</th>
                                <th>Organization</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $data = getListUsers();
                            foreach ($data as $value) {
                                ?>    
                                <tr data-id="<?= $value['id']; ?>">
                                    <td><?= $value['id']; ?></td>
                                    <td><?= $value['last_name']; ?></td>
                                    <td><?= $value['first_name']; ?></td>
                                    <td><?= $value['middle_name']; ?></td>
                                    <td><?= $value['birthday']; ?></td>
                                    <td><?= $value['contact_number']; ?></td>
                                    <td><?= $value['organization']; ?></td>
                                </tr>   
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>


                </div>
                <div class="col-md-6">
                    <legend>User's Information</legend>  
                    <div class="text-center loader">
                        <i class="fs100 mar40 c-green fa fa-play-circle-o fa-4x fa-spin"></i>
                    </div>
                    <div class="user_well">
                        <div class="well">
                            <legend>Basic Information</legend>                    
                            <div id="r-info">                                
                                <form method="" action="">;
                                    <!-- MISSING PARAMETERS (NAME, ID, and VALUE)-->
                                    <div class="row">
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" name="user_last_name" placeholder="Cruz" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="user_first_name" placeholder="Robert" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Middle Name</label>
                                                <input type="text" class="form-control" name="user_middle_name" placeholder="De La" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">                             
                                            <div class="form-group">
                                                <label>User's Address</label>
                                                <input type="text" class="form-control" name="user_address" placeholder="Blk 7. Village Name Street Name. City, Region" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>User's ID</label>
                                                <input type="integer" class="form-control" name="id_user" placeholder="ABC12345" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>User's Birthday</label>
                                                <input type="text" class="form-control" name="user_birthday" placeholder="1990-01-01" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>User's Sex</label>
                                                <input type="text" class="form-control" name="user_gender" placeholder="Male" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>User's Contact Number</label>
                                                <input type="integer" class="form-control" name="user_contact_number" placeholder="XXX-XXX-XXXX" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>User's Organization</label>
                                                <input type="text" class="form-control" name="user_organization" placeholder="ching" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>User's Position</label>
                                                <input type="text" class="form-control" name="user_position" placeholder="Recruit" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">                             
                                            <div class="form-group">
                                                <label>User's Emergency Contact Name</label>
                                                <input type="text" class="form-control" name="user_emergency_name" placeholder="Cruz, Robert De La" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">                             
                                            <div class="form-group">
                                                <label>User's Emergency Contact Number</label>
                                                <input type="text" class="form-control" name="user_emergency_number" placeholder="XXX-XXX-XXXX" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="pull-right">
                                            <a href="" target="" class="btn btn-primary btn-lg disabled"><i class="glyphicon glyphicon-file"></i> View Profile</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <script>
            $(".loader").hide();

            $("body").on("click", "tr", function () {
//        alert($(this).data("id"));
                $(".loader").show();
                $(".cont").hide();
                $.get("http://localhost:8000/user-form?id=" + $(this).data("id"), function (data) {
//            alert("Data: " + data + "\nStatus: " + status);
                    $(".user_well").replaceWith(data);
                    $(".loader").hide();
                });
            });
            $("table").dataTable();
        </script>

        <?php include_once './view/template/footer.php'; ?>