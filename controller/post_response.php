
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"></h1>
            <h1 class="page-header">Fire Recovery Page</h1>


            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="http://localhost:8000/recovery">
                        <?php
                        global $id;
                        $id = $_GET["id"];
                        $data = getFireReport($id);
                        ?>
                        <div class="row">
                            <!-- Using (Alt + 255) Character Space-->
                            <h4 class="sub-header">   Identifiers</h4>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Fire ID</label>
                                    <input type="integer" class="form-control hidden" name="id" value="<?php print_r($data["id"]); ?>" placeholder="123456789" readonly>
                                    <input type="integer" class="form-control" name="id_fire" value="<?php print_r($data["id_fire_report"]); ?>" placeholder="123456789" readonly>
                                </div>
                            </div>
                            <div class="col-lg-8">                             
                                <div class="form-group">
                                    <label>Incident Location</label>
                                    <input type="text" class="form-control" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Reported</label>
                                    <input type="datetime" class="form-control" value="<?php print_r($data["date"]); ?>" placeholder="31 December 2014 18:30:00" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Extinguished</label>
                                    <input type="datetime" class="form-control" value="N/A" placeholder="N/A" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Building Structure</label>
                                    <input type="text" class="form-control" value="<?php print_r($data["bldg_structure"]); ?>" placeholder="Residential" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Additional Information</h4>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="recovery_injuries">Any Additional Injuries?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="" id="rectableaddinj">
                                    <table class="table table-bordered table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Middle Name</th>
                                                <th>Injury Obtained</th>
                                                <th width="5">
                                                    <button id="recoverytableadditionaladdinj" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="recoverytableadditionalinj">
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="recovery_casualties">Any Additional Casualties?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions" id="casualtiesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions" id="casualtiesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="" id="rectableaddcas">
                                    <table class="table table-bordered table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Middle Name</th>
                                                <th>Cause of Death</th>
                                                <th width="5">
                                                    <button id="recoverytableadditionaladdcas" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="recoverytableadditionalcas">
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="recovery_insurance">Is the Building insured?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="insuranceRadioOptions" id="insuranceRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="insuranceRadioOptions" id="insuranceRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="" id="recoverytableadditionalinsurance">
                                    <table class="table table-bordered table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Insurance Number</th>
                                                <th>Insurance Company</th>
                                                <th>Insurance Contact Number</th>
                                                <th>Insurance Representative</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="recoverytableadditionalinsurancebody">
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-warning rrclear"><i class="fa fa-repeat"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="recovery_damages">Estimated Amount of Damages <span class="text-muted c-red" style="font-size:x-small ">in PHP</span><span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="recovery_damages" name="est_damages" placeholder="500000" autofocus="true">                                
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="recovery_families">Estimated Number of Families Affected <span class="c-red"> *</span></label>
                                    <input type="number" class="form-control" id="recovery_families" name="families_affected" placeholder="50">                                
                                </div>
                            </div>
                        </div>
                        <br>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="    Revert    ">                            
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submit" value="    Submit    ">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hidden">
    <table>
        <thead></thead>
        <tbody id="addrr">
            <tr>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="hidden">
    <table>
        <thead></thead>
        <tbody id="addinc">
            <tr>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <button type="button" class="btn btn-xs btn-warning rrclear"><i class="fa fa-repeat"></i></button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<script>
    $("#rectableaddinj").hide();
    $("#rectableaddcas").hide();
    $("#recoverytableadditionalinsurance").hide();

    $("#recoverytableadditionaladdinj").on("click", function () {
        $("#recoverytableadditionalinj").append($("#addrr").html());
    });

    $("#recoverytableadditionaladdcas").on("click", function () {
        $("#recoverytableadditionalcas").append($("#addrr").html());
    });

    $("body").on("click", ".rr", function () {
        $(this).parent().parent().remove();
    });
    $("body").on("click", ".rrclear", function () {
        $(this).parent().parent().remove();
        $("#recoverytableadditionalinsurancebody").append($("#addinc").html());
    });

    method_show_hide_radio('injuriesRadioOptions', "No", "#rectableaddinj")
    method_show_hide_radio('casualtiesRadioOptions', "No", "#rectableaddcas")
    method_show_hide_radio('insuranceRadioOptions', "No", "#recoverytableadditionalinsurance")
</script>
<?php include_once './view/template/footer.php'; ?>