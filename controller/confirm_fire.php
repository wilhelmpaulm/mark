
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Confirmation of Fire and Pre-Deployment</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-6">
<!--                    <table class="table table-hover">
                         On rows 
                        <tr class="active">Currently Fighting</tr>
                        <tr class="success">Fire Out</tr>
                        <tr class="warning">Pending Confirmation</tr>
                        <tr class="danger">IF IGNORED</tr>
                        <tr class="info">Information</tr>

                         On cells (`td` or `th`) 
                        <tr>
                            <td class="active">...</td>
                            <td class="success">...</td>
                            <td class="warning">...</td>
                            <td class="danger">...</td>
                            <td class="info">...</td>
                        </tr>
                    </table>-->
                    <label>Pending, Active, and On-Going Fire</label><br>
                    <span class="text-muted c-black" style="font-size:x-small;">Legend:</span>
                    <span class="label label-danger">Ignored</span>
                    <span class="label label-warning">For Confirmation</span>
                    <span class="label label-success">In Progress</span>                    
<!--                    <span class="label label-info">Confirmed</span>
                    <span class="label label-default">Fire Out</span>--><br>
                    <span class="text-muted c-red" style="font-size:x-small;">Click on the row for additional information</span>
                    <br>
                    <br>
                    <div class="cfto">
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-clock-o"></i> Time</th>
                                    <th><i class="fa fa-map-marker"></i> Exact Location</th>
                                    <th><i class="fa fa-map-marker"></i> City</th>
                                    <th><i class="fa fa-fire"></i> Status</th>
                                    <th><i class="fa fa-user"></i> Confirmed By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $data = getActiveFireReport();
                                foreach ($data as $value) {
                                    if ($value["alarm_status"] != "Fire Out") {
                                        ?>      
                                        <?php if ($value['status'] === 'pending' && (currentdatetime() < date("Y-m-d H:i:s", strtotime($value["date"] . "+5 minutes")))) { ?>
                                            <tr class="warning" data-id="<?= $value['id']; ?>">    
                                            <?php } else if ($value['status'] === 'confirmed') { ?>
                                            <tr class="success" data-id="<?= $value['id']; ?>">
                                            <?php } else if ($value['status'] === 'pending' && (currentdatetime() > date("Y-m-d H:i:s", strtotime($value["date"] . "+5 minutes")))) { ?>
                                            <tr class="danger" data-id="<?= $value['id']; ?>">
                                            <?php } ?>

                                            <td><?= $value['date']; ?></td>
                                            <td><?= $value['address']; ?></td>
                                            <td><?= $value['city']; ?></td>
                                            <td><?= $value['status']; ?></td>
                                            <td><?= $value['confirmed_by']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="col-md-6">
                    <div class="cffo">
                        <div class="text-center loader">
                            <i class="fs100 mar40 c-green fa fa-play-circle-o fa-4x fa-spin"></i>
                        </div>
                        <div class="cont">
                            <legend>Report Information</legend>
                            <div class="well">
                                <div id="r-info">
                                    <form method="POST" action="http://localhost:8000/response">
                                        <div class="row">
                                            <div class="col-lg-4">                             
                                                <div class="form-group">
                                                    <label>Reporter Last Name</label>
                                                    <input type="text" class="form-control" name="reporter_last_name"  placeholder="Cruz" disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">                             
                                                <div class="form-group">
                                                    <label>Reporter First Name</label>
                                                    <input type="text" class="form-control" name="reporter_first_name"  placeholder="Juan" disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">                             
                                                <div class="form-group">
                                                    <label>Reporter Middle Name</label>
                                                    <input type="text" class="form-control"  name="reporter_middle_name"  placeholder="De La" disabled="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">                             
                                                <div class="form-group">
                                                    <label>Exact Address Location</label>
                                                    <input type="text" class="form-control" name="address"  placeholder="Blk 7. Village Name Street Name. City, Region" disabled="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">                             
                                                <div class="form-group">
                                                    <label>Reporter Contact Number</label>
                                                    <input type="integer" class="form-control" name="reporter_contact_number"  placeholder="XXX-XXX-XXXX" disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">                             
                                                <div class="form-group">
                                                    <label>Reporter E-Mail Address</label>
                                                    <input type="email" class="form-control" name="reporter_email_address"  placeholder="me@here.com" disabled="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix hidden">
                                            <div class="pull-right">
                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">
                                                    <i class="glyphicon glyphicon-remove"></i> Decline
                                                </button>
                                                <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal2">
                                                    <i class="glyphicon glyphicon-warning-sign"></i> Conditional Confirm
                                                </button>
                                                <button type="submit" class="btn btn-success btn-lg"><i class="glyphicon glyphicon-ok"></i> Confirm</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->



<script>
    $(".loader").hide();

    $("body").on("click", "tr", function () {
//        alert($(this).data("id"));
        $(".loader").show();
        $(".cont").hide();
        $.get("http://localhost:8000/form-activation?id=" + $(this).data("id"), function (data) {
//            alert("Data: " + data + "\nStatus: " + status);
            $(".cffo").replaceWith(data);
            $(".loader").hide();
        });
    });


    setInterval(function () {
        $.get("http://localhost:8000/table-activation", function (data) {
//            alert("Data: " + data + "\nStatus: " + status);
            $(".cfto").replaceWith(data);
        });
    }, 10000);


</script>   

<?php include_once './view/template/footer.php'; ?>