<div class="cffo">
    <div class="text-center loader">
        <i class="fs60 fa fa-play-circle-o fa-5x fa-spin"></i>
    </div>
    <div class="cont">
        <legend>Report Information</legend>
        <div class="well">
            <div id="r-info">
                <?php
                //isAuthorized();
                global $id, $user;
                $user = $_COOKIE["authorization_user_name"];
                $id = $_GET["id"];
                $data = getReporterInfo($id);
                ?>
                <form method="POST" action="http://localhost:8000/response">
                    <input value="normal" name="type" class="hidden">
                    <input value="<?php global $id; echo $id;?>" name="id" class="hidden">
                    <input value="<?php global $user; echo $user;?>" name="confirmed_by" class="hidden">
                    <div class="row">
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter Last Name</label>
                                <input type="text" class="form-control" name="reporter_last_name" value="<?php print_r($data["reporter_last_name"]); ?>" placeholder="Cruz" readonly>
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter First Name</label>
                                <input type="text" class="form-control" name="reporter_first_name" value="<?php print_r($data["reporter_first_name"]); ?>" placeholder="Juan" readonly>
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter Middle Name</label>
                                <input type="text" class="form-control"  name="reporter_middle_name" value="<?php print_r($data["reporter_middle_name"]); ?>" placeholder="De La" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">                             
                            <div class="form-group">
                                <label>Exact Address Location</label>
                                <input type="text" class="form-control" name="address" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. City, Region" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">                             
                            <div class="form-group">
                                <label>Reporter Contact Number</label>
                                <input type="integer" class="form-control" name="reporter_contact_number" value="<?php print_r($data["reporter_contact_number"]); ?>" placeholder="XXX-XXX-XXXX" readonly>
                            </div>
                        </div>
                        <div class="col-lg-6">                             
                            <div class="form-group">
                                <label>Reporter E-Mail Address</label>
                                <input type="email" class="form-control" name="reporter_email_address" value="<?php print_r($data["reporter_email_address"]); ?>" placeholder="me@here.com" readonly>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($data["status"] === "confirmed" && $data["confirmed_by"] !== null) {
                        ?>
                        <div class="clearfix">
                            <div class="pull-right">
                                <a href="http://localhost:8000/edit?id=<?php echo $data["id_fire_report"] ?>" target="_blank" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-file"></i> Update</a>
                            </div>
                        </div>
                    <?php } else { ?>                    
                        <div class="clearfix" id="buttons">
                            <div class="pull-right">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">
                                    <i class="glyphicon glyphicon-remove"></i> Decline
                                </button>
                                <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal2">
                                    <i class="glyphicon glyphicon-warning-sign"></i> Conditional Confirm
                                </button>
                                <button type="submit" class="btn btn-success btn-lg"><i class="glyphicon glyphicon-ok"></i> Confirm</button>
                            </div>
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Decline?</h4>
                </div>
                <form action="http://localhost:8000/response" method="POST">
                    <input value="decline" name="type" class="hidden">
                    <input value="<?php global $id; echo $id;?>" name="id" class="hidden">
                    <input value="<?php global $user; echo $user;?>" name="confirmed_by" class="hidden">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2">                             
                                <div class="form-group">
                                    <label for="confirm_decline_options">Reason:<span class="c-red"> *</span></label>
                                    <select class="form-control" name="confirm_decline" id="confirm_decline_options">

                                    </select>

                                </div>
                                <br>
                                <div class="form-group " id="rd">
                                    <label for="reason_decline">Please Specify:<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" name="reason_decline" required="" id="reason_decline" value=" " placeholder="Why?"/>
                                </div>
                            </div>
                        </div>
                        <p><span class="col-lg-offset-2 c-red fs20"> * </span> -- required fields</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="rdc" class="btn btn-warning" >Clear</button>
                        <button type="submit" id="rds" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal2 (Conditional Confirm)-->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Fire Incident Location</h4>
                </div>
                <form action="http://localhost:8000/response" method="POST">
                    <input value="conditional" name="type" class="hidden">
                    <input value="<?php global $id; echo $id;?>" name="id" class="hidden">
                    <input value="<?php global $user; echo $user;?>" name="confirmed_by" class="hidden">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Current Address Location</label>
                            <input type="text" class="form-control" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. City, Region" readonly>
                        </div>
                        <div class="form-group">
                            <label for="addressLocation">Incident Location<span class="c-red"> *</span></label>
                            <p class="help-block">Please be <span class="c-red">SPECIFIC</span> as possible</p>
                            <input type="text" class="form-control" name="address" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. City, Region" autofocus>
                        </div>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="rds" class="btn btn-primary">Save and Confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("#rd").hide();
    $("#rds").hide();
    $("#rdc").hide();
    method_select_dropdown(null, "confirm_decline_options", options_confirm_decline);
//shows or removes the option in reason for declining (Others)
    $('#confirm_decline_options').on("change", function () {
        var value = $(this).val();
        if (value == 'Others') {
            $("#rd").show();
            $("#rdc").show();

        } else {
            $("#rd").hide();
            $("#rdc").hide();
        }
    });

    //shows or removes the option in reason for declining (Others)
    $('#confirm_decline_options').on("change", function () {
        var value = $(this).val();
        if (value == 'Others') {
            $("#rd").show();
            $("#reason_decline").val("").focus();
        } else {
            $("#reason_decline").val(" ");
            $("#rd").hide();
        }
        if (value != 'Please Select One!') {
            $("#rds").show();
        } else {
            $("#rds").hide();
        }
    });

    $("#rdc").on("click", function () {
        $("#reason_decline").val("");
    });
</script>