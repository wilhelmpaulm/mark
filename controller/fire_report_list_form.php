<div class="cffo">
    <div class="text-center loader">
        <i class="fs100 mar40 c-green fa fa-play-circle-o fa-4x fa-spin"></i>
    </div>
    <legend>Detail Information</legend>
    <div class="well">
        <div id="r-info">
            <?php
            //isAuthorized();
            global $id;
            $id = $_GET["id"];
            $data = getFireReport($id);
            ?>
            <!--Add Another FORM if $data["status"] is DECLINED-->
            <?php if ($data["status"] !== "declined") { ?>
                <form>
                    <div class="row">
                        <div class="col-lg-5">                             
                            <div class="form-group">
                                <label>Report Status</label>
                                <input type="text" class="form-control" value="<?php print_r($data["status"]); ?>" placeholder="Confirmed" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-7">                             
                            <div class="form-group">
                                <label>DateTime</label>
                                <input type="datetime" class="form-control" value="<?php print_r($data["date"]); ?>" placeholder="31 December 2014 08:30:00" disabled="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter Last Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reporter_last_name"]); ?>" placeholder="Cruz" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter First Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reporter_first_name"]); ?>" placeholder="Juan" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter Middle Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reporter_middle_name"]); ?>" placeholder="De La" disabled="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">                             
                            <div class="form-group">
                                <label>Fire ID</label>
                                <input type="integer" class="form-control" value="<?php print_r($data["id"]); ?>" placeholder="123456789" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-6">                             
                            <div class="form-group">
                                <label>Alarm Status</label>
                                <input type="text" class="form-control" value="<?php print_r($data["alarm_status"]); ?>" placeholder="" disabled="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">                             
                            <div class="form-group">
                                <label>Exact Address Location</label>
                                <input type="text" class="form-control" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. City, Region" disabled="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Owner's Last Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["owner_last_name"]); ?>" placeholder="Cruz" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Owner's First Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["owner_first_name"]); ?>" placeholder="Juan" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Owner's Middle Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["owner_middle_name"]); ?>" placeholder="De La" disabled="true">
                            </div>
                        </div>
                    </div>
                </form>
            <?php } else { ?>
                <form>
                    <div class="row">
                        <div class="col-lg-5">                             
                            <div class="form-group">
                                <label>Report Status</label>
                                <input type="text" class="form-control" value="<?php print_r($data["status"]); ?>" placeholder="Confirmed" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-7">                             
                            <div class="form-group">
                                <label>DateTime</label>
                                <input type="datetime" class="form-control" value="<?php print_r($data["date"]); ?>" placeholder="31 December 2014 08:30:00" disabled="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter Last Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reporter_last_name"]); ?>" placeholder="Cruz" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter First Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reporter_first_name"]); ?>" placeholder="Juan" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-4">                             
                            <div class="form-group">
                                <label>Reporter Middle Name</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reporter_middle_name"]); ?>" placeholder="De La" disabled="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">                             
                            <div class="form-group">
                                <label>Reason of Decline</label>
                                <input type="text" class="form-control" value="<?php print_r($data["confirm_decline"]); ?>" placeholder="" disabled="true">
                            </div>
                        </div>
                        <div class="col-lg-6">                             
                            <div class="form-group">
                                <label>Other Information</label>
                                <input type="text" class="form-control" value="<?php print_r($data["reason_decline"]); ?>" placeholder="" disabled="true">
                            </div>
                        </div>
                    </div>                    
                <?php } ?>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                <a href="http://localhost:8000/viewreport?id=<?php echo $data["id_fire_report"] ?>" target="_blank" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-file"></i> View</a>
            </div>
        </div>
    </div>
</div>