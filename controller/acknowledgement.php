﻿
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row text-center">   
        <br>
        <h2>Thank You For Your Report</h2>       
        <br>
        <p class="small c-red">Re-directing to Home Page in <span id="ss">6</span> Seconds.</p>
    </div>
</div>

<script>
    var ss = $("#ss");
    var sc = $("#ss").text();


    var timer = setInterval(function () {
        if(sc > 0){
            sc--;
            ss.text(sc);
        }else{
             window.clearTimeout(timer);
            window.location.replace("http://localhost:8000");
        }
    }, 1000);

</script>

<?php include_once './view/template/footer.php'; ?>