<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->
<?php
    //isAuthorized();
    $data = getFireReportRestricted($_GET["id"]);
?>
<div class="main">
    <div class="row">
        <h4 class="sub-header">   Viewing Report</h4>
        <div class="col-lg-3">                             
            <div class="form-group">
                <label>Fire ID</label>
                <input type="integer" name="id" value="<?php print_r($data["id"]); ?>" class="form-control" placeholder="123456789" disabled="true">
            </div>
        </div>
        <div class="col-lg-8">                             
            <div class="form-group">
                <label>Incident Location</label>
                <input type="text" name="address" value="<?php print_r($data["address"]); ?>" class="form-control" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" disabled="true">
            </div>
        </div>
        <div class="col-lg-3">                             
            <div class="form-group">
                <label>Alarm Status</label>
                <input type="text" name="alarm_status" class="form-control" placeholder="" value="HIDDEN" disabled="true" id="identifier_alarm_status">
            </div>
        </div>
        <div class="col-lg-8">                             
            <div class="form-group">
                <label>Date Reported</label>
                <input type="datetime" name="date" value="<?php print_r($data["date"]); ?>" class="form-control" placeholder="31 December 2014 18:30:00" disabled="true">
            </div>
        </div>        
    </div>    
</div>
