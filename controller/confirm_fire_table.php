<div class="cfto">
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th><i class="fa fa-clock-o"></i> Time</th>
                <th><i class="fa fa-map-marker"></i> Exact Location</th>
                <th><i class="fa fa-map-marker"></i> City</th>
                <th><i class="fa fa-fire"></i> Status</th>
                <th><i class="fa fa-user"></i> Confirmed By</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $data = getActiveFireReport();
            foreach ($data as $value) {
                if ($value["alarm_status"] != "Fire Out") {
                    ?>      
                    <?php if ($value['status'] === 'pending' && (currentdatetime() < date("Y-m-d H:i:s", strtotime($value["date"] . "+5 minutes")))) { ?>
                        <tr class="warning" data-id="<?= $value['id']; ?>">    
                        <?php } else if ($value['status'] === 'confirmed') { ?>
                        <tr class="success" data-id="<?= $value['id']; ?>">
                        <?php } else if ($value['status'] === 'pending' && (currentdatetime() > date("Y-m-d H:i:s", strtotime($value["date"] . "+5 minutes")))) { ?>
                        <tr class="danger" data-id="<?= $value['id']; ?>">
                        <?php } ?>
                        <td class="hidden cftoid"><?= $value['id']; ?></td>
                        <td><?= $value['date']; ?></td>
                        <td><?= $value['address']; ?></td>
                        <td><?= $value['city']; ?></td>
                        <td><?= $value['status']; ?></td>
                        <td><?= $value['confirmed_by']; ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
