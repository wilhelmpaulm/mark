
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">List of ALL Fire Reports</h1>
            <!-- WE NEED A DATE SELECTOR FEATURE-->

            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-6">
                    <span class="text-muted c-black" style="font-size:x-small;">Legend:</span>
                    <span class="label label-danger">Ignored</span>
                    <span class="label label-warning">For Confirmation</span>
                    <span class="label label-success">In Progress</span>
                    <span class="label label-default">Fire Out / Declined</span><br>
                    <span class="text-muted c-red" style="font-size:x-small;">Click on the row for additional information</span>
                    <br>
                    <br>
                    <div role="tabpanel">
                        <!--NEED A NEW FILE FOR THIS ONE-->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#all" aria-controls="listall" role="tab" data-toggle="tab">List All</a></li>
                            <li role="presentation"><a href="#active" aria-controls="listactive" role="tab" data-toggle="tab">All Active</a></li>
                            <li role="presentation"><a href="#fire_out" aria-controls="listinactive" role="tab" data-toggle="tab">All Fire Out</a></li>
                            <li role="presentation"><a href="#declined" aria-controls="listdeclined" role="tab" data-toggle="tab">All Declined</a></li>
                        </ul>
                        <table class="table table-bordered table-hover table-responsive">
                            <thead>
                                <tr>                                   
                                    <th><i class="fa fa-map-marker"></i> Date</th>
                                    <th> Exact Address</th>
                                    <th> Report Status</th>
                                    <th> Alarm Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $data = getListFireReportAll();
                                foreach ($data as $value) {
                                    ?>      
                                    <?php if ($value['status'] === 'pending' && (currentdatetime() < date("Y-m-d H:i:s", strtotime($value["date"] . "+5 minutes")))) { ?>
                                        <tr class="warning" data-id="<?= $value['id_fire_report']; ?>">      
                                        <?php } else if ($value['status'] === 'confirmed' && $value['alarm_status'] !== 'Fire Out') { ?>
                                        <tr class="success" data-id="<?= $value['id_fire_report']; ?>">
                                        <?php } else if ($value['status'] === 'pending' && (currentdatetime() > date("Y-m-d H:i:s", strtotime($value["date"] . "+5 minutes")))) { ?>
                                        <tr class="danger" data-id="<?= $value['id_fire_report']; ?>">
                                        <?php } else { ?>
                                        <tr class="" data-id="<?= $value['id_fire_report']; ?>">
                                        <?php } ?>
                                        <td><?= $value['date']; ?></td>
                                        <td><?= $value['address']; ?></td>
                                        <td><?= $value['status']; ?></td>
                                        <td><?= $value['alarm_status']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="cffo">
                        <div class="text-center loader">
                            <i class="fs100 mar40 c-green fa fa-play-circle-o fa-4x fa-spin"></i>
                        </div>
                        <legend>Detail Information</legend>
                        <div class="well">
                            <div id="r-info">
                                <form method="GET" action="">
                                    <div class="row">
                                        <div class="col-lg-5">                             
                                            <div class="form-group">
                                                <label>Report Status</label>
                                                <input type="text" class="form-control" placeholder="Confirmed" disabled="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-7">                             
                                            <div class="form-group">
                                                <label>DateTime</label>
                                                <input type="datetime" class="form-control" placeholder="31 December 2014 08:30:00" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Reporter Last Name</label>
                                                <input type="text" class="form-control" placeholder="Cruz" disabled="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Reporter First Name</label>
                                                <input type="text" class="form-control" placeholder="Juan" disabled="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Reporter Middle Name</label>
                                                <input type="text" class="form-control" placeholder="De La" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">                             
                                            <div class="form-group">
                                                <label>Fire ID</label>
                                                <input type="integer" class="form-control" placeholder="123456789" disabled="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">                             
                                            <div class="form-group">
                                                <label>Alarm Status</label>
                                                <input type="text" class="form-control" placeholder="3th Alarm" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">                             
                                            <div class="form-group">
                                                <label>Exact Address Location</label>
                                                <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. City, Region" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Owner's Last Name</label>
                                                <input type="text" class="form-control" placeholder="Cruz" disabled="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Owner's First Name</label>
                                                <input type="text" class="form-control" placeholder="Juan" disabled="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">                             
                                            <div class="form-group">
                                                <label>Owner's Middle Name</label>
                                                <input type="text" class="form-control" placeholder="De La" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script>
            $(".loader").hide();

            $("body").on("click", "tr", function () {
//        alert($(this).data("id"));
                $(".loader").show();
                $(".cont").hide();
                $.get("http://localhost:8000/form-list?id=" + $(this).data("id"), function (data) {
//            alert("Data: " + data + "\nStatus: " + status);
                    $(".cffo").replaceWith(data);
                    $(".loader").hide();
                });
            });

            $("table").dataTable();

        </script>

        <?php include_once './view/template/footer.php'; ?>