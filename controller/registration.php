
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"></h1>
            <h1 class="page-header">Welcome New User</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <!--                        <div class="form-group">
                                                    <label for="addressLocation">Incident Location<span class="c-red"> *</span></label>
                                                    <p class="help-block">Please be <span class="c-red">SPECIFIC</span> as possible</p>
                                                    <input type="text" class="form-control" id="addressLocation" placeholder="Blk 7. Village Name Street Name." autofocus="true">
                                                </div>-->
                        <!--                            <p> Barangay, City Region </p>-->
                        <div class="row">
                            <!-- Using (Alt + 255) Character Space-->
                            <h4 class="sub-header">   Identifiers</h4>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Your Unique ID</label>
                                    <input type="integer" class="form-control" placeholder="ABC12345" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-8">                             
                                <div class="form-group">
                                    <label>Task Force Address</label>
                                    <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Your Task Force Name</label>
                                    <input type="text" class="form-control" placeholder="Victory" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Task Force Organization</label>
                                    <input type="text" class="form-control" placeholder="TxT Fire Organization" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Position</label>
                                    <input type="text" class="form-control" placeholder="Volunteer" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Critical Information</h4>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="user_password">Password<span class="c-red"> *</span></label>
                                    <input type="password" class="form-control" id="user_last_name" placeholder="***************">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="user_password">Secret Question<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="user_sec_question" placeholder="Create your own Secret Question">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="user_password">Secret Answer<span class="c-red"> *</span></label>
                                    <input type="password" class="form-control" id="user_sec_password" placeholder="***************">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Essential Information</h4>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="user_last_name">Last Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="user_last_name" placeholder="Cruz">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="user_first_name">First Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="user_first_name" placeholder="Robert">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="user_middle_name">Middle Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="user_middle_name" placeholder="De La">
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="user_phone">Phone Number <span class="text-muted c-red" style="font-size:x-small ">Area Code Included</span><span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="user_contact" placeholder="XX-XXX-XXXX">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="user_mobile">Mobile Number<span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="user_contact" placeholder="XXX-XXX-XXXX">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="user_email">Email Address<span class="c-red"> *</span></label>
                                    <input type="email" class="form-control" id="user_email" placeholder="me@here.com">                                
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="user_birthday">Date of Birth<span class="c-red"> *</span></label>
                                    <input type="date" class="form-control" id="user_contact" value="1992-01-29" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label>Gender<span class="c-red"> *</span></label>
                                <div class="form-group">
                                    <div style="padding-top: 7px;">
                                    <label class="radio-inline">
                                        <input type="radio" name="sexRadioOptions" id="sexRadio1" value="Male">Male
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="sexRadioOptions" id="sexRadio2" value="Female">Female
                                    </label>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <br>
                            <h4 class="sub-header">   In Case of Emergency</h4>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="em_user_last_name">Last Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="em_user_last_name" placeholder="Cruz">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="em_user_first_name">First Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="em_user_first_name" placeholder="Robert">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="em_user_middle_name">Middle Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="em_user_middle_name" placeholder="De La">
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="em_user_phone">Phone Number <span class="text-muted c-red" style="font-size:x-small ">Area Code Included</span><span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="em_user_contact" placeholder="XX-XXX-XXXX">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="em_user_mobile">Mobile Number<span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="em_user_contact" placeholder="XXX-XXX-XXXX">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="em_user_email">Email Address<span class="c-red"> *</span></label>
                                    <input type="email" class="form-control" id="em_user_email" placeholder="me@here.com">                                
                                </div>
                            </div>
                        </div>
                        <br>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>                       
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="     Reset     ">                            
                        <div id="buttons">
                            <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submit" value="      Save      ">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <table>
        <thead>

        </thead>
        <tbody id="addrr">
            <tr>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
        </tbody>
    </table>

</div>
<script>
    $("#buttons").hide();
    $("#ra").on("click", function () {
        $("#rt").append($("#addrr").html());
    });

    $("#rc").on("click", function () {
        $("#rtc").append($("#addrr").html());
    });

    $("body").on("click", ".rr", function () {
        $(this).parent().parent().remove();
    });

    $("input:radio[name=sexRadioOptions]").on("click", function () {
        $("#buttons").show();
    });
</script>


<?php include_once './view/template/footer.php'; ?>