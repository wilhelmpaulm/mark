<div class="user_well">
    <div class="well">
        <legend>Basic Information</legend>                    
        <div id="r-info">
            <?php
            //isAuthorized();
            global $id, $user;
            $user = $_COOKIE["authorization_user_name"];
            $id = $_GET["id"];
            $data = getUser($id);
            ?>
            <form>;
                <!-- MISSING PARAMETERS (NAME, ID, and VALUE)-->
                <div class="row">
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="user_last_name" value="<?php print_r($data["last_name"]); ?>" placeholder="Cruz" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="user_first_name" value="<?php print_r($data["first_name"]); ?>" placeholder="Robert" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>Middle Name</label>
                            <input type="text" class="form-control" name="user_middle_name" value="<?php print_r($data["middle_name"]); ?>" placeholder="De La" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">                             
                        <div class="form-group">
                            <label>User's Address</label>
                            <input type="text" class="form-control" name="user_address" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. City, Region" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>User's ID</label>
                            <input type="integer" class="form-control" name="id_user" value="<?php print_r($data["id"]); ?>" placeholder="ABC12345" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>User's Birthday</label>
                            <input type="text" class="form-control" name="user_birthday" value="<?php print_r($data["birthday"]); ?>" placeholder="1990-01-01" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>User's Sex</label>
                            <input type="text" class="form-control" name="user_gender" value="<?php print_r($data["gender"]); ?>" placeholder="Male" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>User's Contact Number</label>
                            <input type="integer" class="form-control" name="user_contact_number" value="<?php print_r($data["contact_number"]); ?>" placeholder="XXX-XXX-XXXX" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>User's Organization</label>
                            <input type="text" class="form-control" name="user_organization" value="<?php print_r($data["organization"]); ?>" placeholder="ching" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4">                             
                        <div class="form-group">
                            <label>User's Position</label>
                            <input type="text" class="form-control" name="user_position" value="<?php print_r($data["position"]); ?>" placeholder="Recruit" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">                             
                        <div class="form-group">
                            <label>User's Emergency Contact Name</label>
                            <input type="text" class="form-control" name="user_emergency_name" value="<?php print_r($data["emergency_contact_name"]); ?>" placeholder="Cruz, Robert De La" readonly>
                        </div>
                    </div>
                    <div class="col-lg-6">                             
                        <div class="form-group">
                            <label>User's Emergency Contact Number</label>
                            <input type="text" class="form-control" name="user_emergency_number" value="<?php print_r($data["emergency_contact_number"]); ?>" placeholder="XXX-XXX-XXXX" readonly>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="pull-right">
                    <a href="http://localhost:8000/profile?id=<?php echo $data["id"] ?>" target="_blank" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-file"></i> View Profile</a>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>
