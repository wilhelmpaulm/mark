<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->
<?php
    //isAuthorized();
    $data = getFireReport($_GET["id"]);
?>
<div class="main">
    <div class="row">
        <h4 class="sub-header">   Viewing Report</h4>
        <div class="col-lg-3">                             
            <div class="form-group">
                <label>Fire ID</label>
                <input type="integer" name="id" value="<?php print_r($data["id"]); ?>" class="form-control" placeholder="123456789" disabled="true">
            </div>
        </div>
        <div class="col-lg-8">                             
            <div class="form-group">
                <label>Incident Location</label>
                <input type="text" name="address" value="<?php print_r($data["address"]); ?>" class="form-control" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" disabled="true">
            </div>
        </div>
        <div class="col-lg-3">                             
            <div class="form-group">
                <label>Alarm Status</label>
                <input type="text" name="alarm_status" class="form-control" placeholder=""  value="<?php print_r($data["alarm_status"]); ?>"" disabled="true" id="identifier_alarm_status">
            </div>
        </div>
        <div class="col-lg-4">                             
            <div class="form-group">
                <label>Date Reported</label>
                <input type="datetime" name="date" value="<?php print_r($data["date"]); ?>" class="form-control" placeholder="31 December 2014 18:30:00" disabled="true">
            </div>
        </div>
        <div class="col-lg-4">                             
            <div class="form-group">
                <label>Confirmed By</label>
                <input type="text" class="form-control" value="<?php print_r($data["confirmed_by"]); ?>" placeholder="Mario and Luigi" disabled="true">
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-lg-4">
            <div class="form-group">
                <label for="assessment_oic">Current Officer-In-Charge<span class="c-red"> *</span></label>
                <input type="text" class="form-control" id="assessment_oic" value="<?php print_r($data["officer_in_charge"]); ?>" placeholder="Cruz, Joseph De La" disabled="true">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="assessment_bldg_structure">Building Structure<span class="c-red"> *</span></label>
                <input type="text" class="form-control" id="assessment_bldg_structure" value="<?php print_r($data["bldg_structure"]); ?>" placeholder="Type of Building?" disabled="true">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="assessment_bldg_elevation">Elevation<span class="c-red"> *</span></label>
                <input type="text" class="form-control" id="assessment_bldg_elevation" value="<?php print_r($data["elevation"]); ?>" placeholder="What Floor?" disabled="true">
            </div>
        </div>
    </div>    
    <div class="row">
        <div class="col-lg-11">
            <div class="form-group">
                <label for="assessment_bldg_composition">Building Composition <span class="text-muted c-red" style="font-size:x-small ">Please Be Specific</span><span class="c-red"> *</span></label>
                <input type="text" class="form-control" id="assessment_bldg_composition" value="<?php print_r($data["building_composition"]); ?>" placeholder="What is Burning Inside?" disabled="true">                                    
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-11">            
            <label>injuries Table</label>
            <div class="" id="assessment_injuries_table">
                <table class="table table-bordered table-condensed table-responsive">
                    <thead>
                        <tr>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Injury Obtained</th>
                        </tr>
                    </thead>
                    <tbody id="rt">
                        <tr>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-11">
            <label>Casualties Table</label>
            <div class="" id="assessment_casualties_table">
                <table class="table table-bordered table-condensed table-responsive">
                    <thead>
                        <tr>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Cause of Death</th>
                        </tr>
                    </thead>
                    <tbody id="rtc">
                        <tr>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                            <td>
                                <input class="form-control" type="text" name=" " value="" disabled="true"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label for="assessment_affected_building">Number of Building Affected<span class="c-red"> *</span></label>
                <input type="number" class="form-control" id="assessment_affected_building" value="<?php print_r($data["structures_affected"]); ?>" placeholder="N/A" disabled="true">                                
            </div>
        </div>
        <div class="col-lg-7">
            <div class="form-group">
                <label for="assessment_fire_type">Type of Fire<span class="c-red"> *</span></label>
                <input type="text" class="form-control" id="assessment_fire_type" value="<?php print_r($data["type_fire"]); ?>" placeholder="Type of Fire?" disabled="true">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label for="assessment_cause_fire">Cause of Fire <span class="text-muted c-red" style="font-size:x-small ">Accident or Intentional</span></label>
                <input type="text" class="form-control" id="assessment_cause_fire" value="<?php print_r($data["cause"]); ?>" placeholder="Why did it start?" disabled="true">                                    
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="assessment_fire_origin">Origin of Fire</label>
                <input type="text" class="form-control" id="assessment_fire_origin" value="<?php print_r($data["origin"]); ?>" placeholder="Where did it start?" disabled="true">                                    
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="assessment_fire_source">Additional Info</label>
                <input type="text" class="form-control" id="assessment_fire_source" value="<?php print_r($data["other_info_fire"]); ?>" placeholder="Anything Else?" disabled="true">                                    
            </div>
        </div>
    </div>
</div>
