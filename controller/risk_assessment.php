
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"></h1>
            <h1 class="page-header">Risk-Assessment Page</h1>


            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="http://localhost:8000/edit">
                        <div class="row">
                            <?php
                            global $id;
                            $id = $_GET["id"];
                            $data = getFireReport($id);
                            ?> 
                            <!-- Using (Alt + 255) Character Space-->
                            <h4 class="sub-header">   Identifiers</h4>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Fire ID</label>
                                    <input type="integer" class="form-control hidden" name="id" value="<?php print_r($data["id"]); ?>" placeholder="123456789" readonly>
                                    <input type="integer" class="form-control" name="id_fire" value="<?php print_r($data["id_fire_report"]); ?>" placeholder="123456789" readonly>
                                </div>
                            </div>
                            <div class="col-lg-8">                             
                                <div class="form-group">
                                    <label>Incident Location</label>
                                    <input type="text" class="form-control" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Current Alarm Status</label>
                                    <input type="text" class="form-control" value="<?php print_r($data["alarm_status"]); ?>" placeholder="" id="identifier_alarm_status" readonly >
                                    <input type="text" class="hidden" value="<?php print_r($data["bldg_structure"]); ?>" placeholder="" id="identifier_bldg_structure" readonly >
                                    <input type="text" class="hidden" value="<?php print_r($data["type_fire"]); ?>" placeholder="" id="identifier_type_fire" readonly >
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Reported</label>
                                    <input type="datetime" class="form-control" value="<?php print_r($data["date"]); ?>" placeholder="31 December 2014 18:30:00" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Confirmed By</label>
                                    <input type="text" class="form-control" value="<?php print_r($data["confirmed_by"]); ?>" placeholder="Mario and Luigi" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Critical Information</h4>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_oic">Current Officer-In-Charge<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" name="officer_in_charge" value="<?php print_r($data["officer_in_charge"]); ?>" id="assessment_oic" placeholder="Cruz, Joseph De La" autofocus="true">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_bldg_structure">Building Structure<span class="c-red"> *</span></label>
                                    <select class="form-control" name="building_structure" value="<?php print_r($data["bldg_structure"]); ?>" id="assessment_bldg_structure">

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="assessment_bldg_elevation">Elevation<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" name="elevation" id="assessment_bldg_elevation" value="<?php print_r($data["elevation"]); ?>" placeholder="What Floor?">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_alarm_status">Alarm Status<span class="c-red"> *</span></label>
                                    <select  class="form-control" name="alarm_status" id="assessment_alarm_status" value="<?php print_r($data["alarm_status"]); ?>" >

                                    </select>                          
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_wind_speed">Wind Speed <span class="text-muted c-red" style="font-size:x-small ">in kph</span><span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="assessment_wind_speed" name="wind_speed" value="<?php print_r($data["wind_speed"]); ?>" placeholder="6 kph">                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="assessment_wind_direction">Wind Direction<span class="c-red"> *</span></label>
                                    <select  class="form-control" name="wind_direction" id="assessment_wind_direction" value="<?php print_r($data["wind_direction"]); ?>" >

                                    </select>    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="assessment_bldg_composition">Building Composition <span class="text-muted c-red" style="font-size:x-small ">Please Be Specific</span><span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="assessment_bldg_composition" name="building_composition" value="<?php print_r($data["building_composition"]); ?>" placeholder="What is Burning Inside?">                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Essential Information</h4>                            
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="assessment_injuries">Any Injuries?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="" id="assessment_injuries_table">
                                    <table class="table table-bordered table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Middle Name</th>
                                                <th>Injury Obtained</th>
                                                <th width="5">
                                                    <button id="ra" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="rt">
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="assessment_casualties">Any Casualties?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions" id="casualtiesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions"  id="casualtiesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="" id="assessment_casualties_table">
                                    <table class="table table-bordered table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Middle Name</th>
                                                <th>Cause of Death</th>
                                                <th width="5">
                                                    <button id="rc" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="rtc">
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_affected_building">Number of Structure/s Affected<span class="c-red"> *</span></label>
                                    <input type="number" class="form-control" name="structures_affected" value="<?php print_r($data["structures_affected"]); ?>" id="assessment_affected_building" placeholder="How many structures are burning">                                
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label for="assessment_fire_type">Type of Fire<span class="c-red"> *</span></label>
                                    <select class="form-control" name="type_fire" value="<?php print_r($data["type_fire"]); ?>" id="assessment_fire_type">                                          
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_cause_fire">Cause of Fire <span class="text-muted c-red" style="font-size:x-small ">Accident or Intentional</span></label>
                                    <input type="text" class="form-control" name="cause" id="assessment_cause_fire" value="<?php print_r($data["cause"]); ?>" placeholder="Why did it start?">                                    
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_fire_origin">Origin of Fire</label>
                                    <input type="text" class="form-control" name="origin" id="assessment_fire_origin" value="<?php print_r($data["origin"]); ?>" placeholder="Where did it start?">                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="assessment_fire_source">Additional Info</label>
                                    <input type="text" class="form-control" name="other_info_fire"id="assessment_fire_source" value="<?php print_r($data["other_info_fire"]); ?>" placeholder="Anything Else?">                                    
                                </div>
                            </div>
                        </div>
                        <br>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                        <div id="buttons">
                            <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="    Revert    ">                            
                            <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submitsave" value="   Save Only   ">
                            <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submitbroadcast" value="Broadcast and Save">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <table>
        <thead>

        </thead>
        <tbody id="addrr">
            <tr>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
        </tbody>
    </table>

</div>
<script>
    // THE BROADCAST AND SAVE BUTTON MUST HIDE IF ALARM_STATUS IS SET TO "FIRE OUT"
    
    $("#buttons").hide();
    $("#assessment_injuries_table").hide();
    $("#assessment_casualties_table").hide();

    $("#ra").on("click", function () {
        $("#rt").append($("#addrr").html());
    });

    $("#rc").on("click", function () {
        $("#rtc").append($("#addrr").html());
    });

    $("body").on("click", ".rr", function () {
        $(this).parent().parent().remove();
    });

    method_select_dropdown("identifier_alarm_status", "assessment_alarm_status", options_alarm_status);
    method_select_dropdown(null, "assessment_wind_direction", options_wind_directions);
    method_select_dropdown("identifier_bldg_structure", "assessment_bldg_structure", options_building_structure);
    method_select_dropdown("identifier_type_fire", "assessment_fire_type", options_fire_type);
    method_show_hide(assessment_wind_direction, 'Please Select One!', "#buttons");
    method_show_hide_radio('injuriesRadioOptions', "No", "#assessment_injuries_table")
    method_show_hide_radio('casualtiesRadioOptions', "No", "#assessment_casualties_table")
</script>


<?php include_once './view/template/footer.php'; ?>