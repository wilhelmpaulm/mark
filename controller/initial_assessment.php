
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Pre Risk-Assessment Page</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-9">
                    <?php
                    global $id;
                    $id = $_GET["id"];
                    $data = getFireReport($id);
                    ?>
                        <form method="POST" action="http://localhost:8000/broadcast">
                            <div class="row">
                                <!-- Using (Alt + 255) Character Space-->
                                <h4 class="sub-header">   Identifiers</h4>
                                <div class="col-lg-3">                             
                                    <div class="form-group">
                                        <label>Fire ID</label>
                                        <input type="integer" class="form-control hidden" name="id" value="<?php print_r($data["id"]); ?>" placeholder="123456789" readonly>
                                        <input type="integer" class="form-control" name="id_fire" value="<?php print_r($data["id_fire_report"]); ?>" placeholder="123456789" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-8">                             
                                    <div class="form-group">
                                        <label>Incident Location</label>
                                        <input type="text" class="form-control" value="<?php print_r($data["address"]); ?>" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4">                             
                                    <div class="form-group">
                                        <label>Reporter Contact Number</label>
                                        <input type="text" class="form-control" value="<?php print_r($data["reporter_contact_number"]); ?>" placeholder="XXX-XXX-XXXX" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-3">                             
                                    <div class="form-group">
                                        <label>Date Reported</label>
                                        <input type="datetime" class="form-control" value="<?php print_r($data["date"]); ?>" placeholder="31 December 2014 18:30:00" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4">                             
                                    <div class="form-group">
                                        <label>Confirmed By</label>
                                        <input type="text" class="form-control" value="<?php print_r($data["confirmed_by"]); ?>" placeholder="Mario and Luigi" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="sub-header">   Initial Information</h4>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="respond_oic">Current Officer-In-Charge</label>
                                        <input type="text" class="form-control" id="respond_oic" name="officer_in_charge" placeholder="Cruz, Joseph De La" autofocus="true">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="respond_bldg_structure">Building Structure<span class="c-red"> *</span></label>
                                        <select class="form-control" name="building_structure" id="respond_bldg_structure">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="respond_bldg_elevation">Elevation</label>
                                        <input type="text" class="form-control" id="respond_bldg_elevation" name="elevation" placeholder="What Floor?">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="respond_alarm_status">Alarm Status<span class="c-red"> *</span></label>
                                        <select class="form-control" name="alarm_status" id="respond_alarm_status">

                                        </select>                          
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="respond_wind_speed">Wind Speed <span class="text-muted c-red" style="font-size:x-small ">in kph</span></label>
                                        <input type="integer" class="form-control" id="respond_wind_speed" name="wind_speed" placeholder="6 kph">                                    
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="respond_wind_direction">Wind Direction<span class="c-red"> *</span></label>
                                        <select class="form-control" name="wind_direction" id="respond_wind_direction">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11">
                                    <div class="form-group">
                                        <label for="respond_bldg_composition">Building Composition <span class="text-muted c-red" style="font-size:x-small ">Please Be Specific</span></label>
                                        <input type="text" class="form-control" id="respond_bldg_composition" name="building_composition" placeholder="What is Burning Inside?">                                    
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p><span class="c-red fs20"> * </span> -- required fields</p>
                            <div id="buttons">                        
                                <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="    Revert    ">                            
                                <input class="btn btn-primary pull-right" type="submit" name="submit" value="  Broadcast  ">              
                            </div>
                        </form>                       
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#buttons").hide();
    method_select_dropdown(null, "respond_alarm_status", options_alarm_status_initial);
    method_select_dropdown(null, "respond_wind_direction", options_wind_directions);
    method_select_dropdown(null, "respond_bldg_structure", options_building_structure);
    method_show_hide(respond_wind_direction, 'Please Select One!', "#buttons");
</script>

<?php include_once './view/template/footer.php'; ?>