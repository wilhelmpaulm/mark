--
-- MySQL 5.6.15
-- Tue, 30 Jun 2015 03:28:08 +0000
--

CREATE DATABASE `ching` DEFAULT CHARSET utf8;

USE `ching`;

CREATE TABLE `beds` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `room` varchar(45),
   `bed` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `beds` is empty]

CREATE TABLE `document` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `name` varchar(45),
   `file` varchar(45),
   `type` varchar(45),
   `status` varchar(45),
   `date_issued` varchar(45),
   `date_paid` varchar(45),
   `payment_code` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `document` is empty]

CREATE TABLE `document_request` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `payment_code` varchar(45),
   `type` varchar(45),
   `status` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `document_request` is empty]

CREATE TABLE `forum_question` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_user` varchar(45),
   `question` varchar(45),
   `type` varchar(45),
   `status` varchar(45),
   `vote_up` text,
   `vote_down` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `forum_question` is empty]

CREATE TABLE `forum_reply` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_user` varchar(45),
   `id_question` varchar(45),
   `reply` text,
   `type` varchar(45),
   `status` varchar(45),
   `vote_up` text,
   `vote_down` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `forum_reply` is empty]

CREATE TABLE `patient_abstracts` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `date_admitted` varchar(45),
   `date_discharged` varchar(45),
   `brief_clinical_history` text,
   `dispositions` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `patient_abstracts` is empty]

CREATE TABLE `patient_admitting_diagnosis` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `id_patient_abstract` varchar(45),
   `diagnosis` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `patient_admitting_diagnosis` is empty]

CREATE TABLE `patient_blood` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `id_medtech` varchar(45),
   `id_nurse` varchar(45),
   `type_patient` text,
   `type_donor` varchar(45),
   `unit` varchar(45),
   `isolation` varchar(45),
   `date` varchar(45),
   `time` varchar(45),
   `packed_rbc` varchar(45),
   `cryopt` varchar(45),
   `proper_plasma` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `patient_blood` is empty]

CREATE TABLE `patient_chief_complaints` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `id_patient_abstract` varchar(45),
   `complaint` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `patient_chief_complaints` is empty]

CREATE TABLE `patient_demographics` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` int(11),
   `id_doctor` int(11),
   `id_appointment` varchar(45),
   `weight` varchar(45),
   `height` varchar(45),
   `pulse` varchar(45),
   `respiration` varchar(45),
   `systolic` varchar(45),
   `diastolic` varchar(45),
   `temperature` varchar(45),
   `companion_name` varchar(45),
   `companion_complaint` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

INSERT INTO `patient_demographics` (`id`, `date_created`, `date_updated`, `id_patient`, `id_doctor`, `id_appointment`, `weight`, `height`, `pulse`, `respiration`, `systolic`, `diastolic`, `temperature`, `companion_name`, `companion_complaint`) VALUES 
('1', '2015-03-25 15:08:15', '0000-00-00 00:00:00', '1', '', '', 'sad', 'asd', 'dasd', 'asdas', 'asdas', 'dasd', 'das', '', ''),
('2', '2015-06-12 13:53:05', '0000-00-00 00:00:00', '2', '', '', '', '', '', '', '', '', '', '', '');

CREATE TABLE `patient_illness_histories` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `illness` varchar(45),
   `remarks` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `patient_illness_histories` is empty]

CREATE TABLE `patient_medication` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient_prescription` varchar(45),
   `medication` varchar(45),
   `frequency` varchar(45),
   `dose` varchar(45),
   `route` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

INSERT INTO `patient_medication` (`id`, `date_created`, `date_updated`, `id_patient_prescription`, `medication`, `frequency`, `dose`, `route`) VALUES 
('1', '2015-06-19 04:33:54', '0000-00-00 00:00:00', '1', 'asd', '456', '324', 'sdf'),
('2', '2015-06-19 04:36:38', '0000-00-00 00:00:00', '2', 'fdgdf', '2342', '234', 'dvdf'),
('3', '2015-06-19 04:36:38', '0000-00-00 00:00:00', '2', 'fdgdf', '234', '324', 'gdgfd'),
('4', '2015-06-19 04:36:38', '0000-00-00 00:00:00', '2', 'dfgdf', '324', '324', 'gdfg');

CREATE TABLE `patient_prescription` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `id_doctor` varchar(45),
   `status` varchar(45),
   `remarks` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

INSERT INTO `patient_prescription` (`id`, `date_created`, `date_updated`, `id_patient`, `id_doctor`, `status`, `remarks`) VALUES 
('1', '2015-06-19 04:33:54', '0000-00-00 00:00:00', '1', '5', '', 'rfgdsgfd'),
('3', '2015-06-19 04:36:59', '0000-00-00 00:00:00', '1', '5', '', 'dsfdf'),
('4', '2015-06-19 04:38:57', '0000-00-00 00:00:00', '1', '5', '', 'dsfdf');

CREATE TABLE `patient_vitals` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_round` varchar(45),
   `id_doctor` varchar(45),
   `id_patient` int(11),
   `remarks` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `patient_vitals` is empty]

CREATE TABLE `patients` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `password` varchar(45),
   `last_name` varchar(45),
   `middle_name` varchar(45),
   `first_name` varchar(45),
   `birthday` date,
   `gender` varchar(45),
   `contact_number` varchar(45),
   `email` varchar(45),
   `username` varchar(45),
   `occupation` varchar(45),
   `religion` varchar(45),
   `nationality` varchar(45),
   `address` text,
   `blood_type` varchar(45),
   `patient_number` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

INSERT INTO `patients` (`id`, `date_created`, `date_updated`, `password`, `last_name`, `middle_name`, `first_name`, `birthday`, `gender`, `contact_number`, `email`, `username`, `occupation`, `religion`, `nationality`, `address`, `blood_type`, `patient_number`) VALUES 
('1', '2015-03-25 15:08:15', '0000-00-00 00:00:00', 'root', 'dasd', 'asdas', 'asd', '0000-00-00', 'male', 'sdasd', 'asd@asd.asd', 'asd@asd.asd', 'das', '', '', 'asda', 'A+', ''),
('2', '2015-06-12 13:53:05', '0000-00-00 00:00:00', 'root', 'ks', '', '', '0000-00-00', 'male', '', '', '', '', '', '', '', 'A+', '');

CREATE TABLE `room_assignments` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_patient` varchar(45),
   `id_doctor` varchar(45),
   `id_nurse` varchar(45),
   `id_fellow` varchar(45),
   `id_room` varchar(45),
   `id_bed` varchar(45),
   `date_start` datetime,
   `date_end` datetime,
   `type` varchar(45),
   `triange` varchar(45),
   `status` varchar(45),
   `remarks` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

INSERT INTO `room_assignments` (`id`, `date_created`, `date_updated`, `id_patient`, `id_doctor`, `id_nurse`, `id_fellow`, `id_room`, `id_bed`, `date_start`, `date_end`, `type`, `triange`, `status`, `remarks`) VALUES 
('1', '2015-03-25 15:12:52', '2015-03-25 15:13:06', '5', '1', '3', '', '1', '1', '2015-03-02 15:12:46', '2015-03-25 15:12:48', 'consultation', '1', 'confirmed', 'dfdsaf'),
('2', '2015-06-19 06:03:56', '0000-00-00 00:00:00', '1', '2', '3', '', '101', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'consultation', '1', 'confirmed', '');

CREATE TABLE `rooms` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `room` varchar(45),
   `ward` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `rooms` is empty]

CREATE TABLE `rounds` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_` varchar(45),
   `id_doctor` varchar(45),
   `id_room` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `rounds` is empty]

CREATE TABLE `templates` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `templates` is empty]

CREATE TABLE `user_schedules` (
   `id` int(11) not null auto_increment,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `id_user` varchar(45),
   `day` varchar(45),
   `time_start` varchar(45),
   `time_end` varchar(45),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `user_schedules` is empty]

CREATE TABLE `users` (
   `id` int(11) not null auto_increment,
   `picture` text,
   `date_created` timestamp not null default CURRENT_TIMESTAMP,
   `date_updated` timestamp not null default '0000-00-00 00:00:00',
   `password` varchar(45),
   `last_name` varchar(45),
   `first_name` varchar(45),
   `birthday` date,
   `gender` varchar(45),
   `fb_id` text,
   `fb_link` text,
   `contact_number` varchar(45),
   `email` varchar(45),
   `username` varchar(45),
   `secret_question` text,
   `secret_answer` text,
   `type` varchar(45),
   `patient_number` text,
   `resume` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=8;

INSERT INTO `users` (`id`, `picture`, `date_created`, `date_updated`, `password`, `last_name`, `first_name`, `birthday`, `gender`, `fb_id`, `fb_link`, `contact_number`, `email`, `username`, `secret_question`, `secret_answer`, `type`, `patient_number`, `resume`) VALUES 
('1', '', '2015-03-25 15:08:14', '0000-00-00 00:00:00', 'root', 'dasd', 'asd', '0000-00-00', 'male', '', '', '', 'asd@asd.asd', 'asd@asd.asd', '', '', 'patient', '', ''),
('2', '', '2015-03-25 15:27:48', '0000-00-00 00:00:00', 'root', 'Chang', 'Sueu', '1860-02-02', 'male', '', '', '', 'markryan@gmail.com', 'doctor', '', '', 'doctor', '', ''),
('3', '', '2015-03-25 15:27:48', '0000-00-00 00:00:00', 'root', 'Rhianna', 'Mana', '1860-02-02', 'male', '', '', '', 'markryan@gmail.com', 'nurse', '', '', 'nurse', '', ''),
('4', '', '2015-03-25 15:27:48', '0000-00-00 00:00:00', 'root', 'Ryana', 'Marky', '1860-02-02', 'male', '', '', '', 'markryan@gmail.com', 'medtech', '', '', 'medtech', '', ''),
('5', '', '2015-06-12 10:59:08', '0000-00-00 00:00:00', 'root', 'lala', 'mama', '2015-12-31', 'male', '', '', '', '', 'admin', '', '', 'admin', '', ''),
('7', '', '2015-06-12 13:53:05', '0000-00-00 00:00:00', 'root', 'ks', '', '0000-00-00', 'male', '', '', '', '', '', '', '', 'patient', '', '');