<?php

include './lib/db.php';
include './lib/AltoRouter.php';


//$user = [
//    "id" => "1",
//    "last_name" => "Ryan",
//    "first_name" => "Mark",
//    "email" => "markryan@gmail.com",
//    "username" => "markryan",
//    "gender" => "male",
//    "type" => "admin"
//        //admin, doctor, nurse, medtech, 
//];
//
//toSession("user", $user);
$router = new AltoRouter();

//ALL EXPLICIT ROUTES HERE

$router->map('GET', '/', function() {
//    var_dump(getTable("users", ["email" => "wilhelmpaulm@gmail.com"]));
    sendTo("login");
//    include linkPage("ching/index");
});

$router->map('GET', '/register', function() {
    include linkPage("ching/register");
});

$router->map('GET', '/register/fb', function() {
    include "./lib/fb.php";
});

$router->map('GET', '/login', function() {
    if (fromSession("user") != null) {
        sendTo("schedules");
    }
    include linkPage("ching/login");
});

$router->map('POST', '/login', function() {
    $data = getPost();
//    var_dump($data);
//    die
    $user = getTable("users", ["AND" => ["username" => $data["email"], "password" => $data["password"]]]);
    if ($user != null) {
        toSession("user", $user);
    } else {
        sendTo("logout");
    }
    if ($user["type"] == "patient") {

        $patient = "";
        $patients = selectTable("patients");
        foreach ($patients as $p) {
            if ($p["email"] == $user["email"]) {
                $patient = $p;
            }
        }
        toSession("puser", $patient);
        sendTo("patients/" . $patient["id"]);
    } else {

        sendTo("schedules");
    }
});

$router->map('POST', '/register', function() {
    $data = getPost();
    $user = [
        "username" => $data["username"],
        "password" => $data["password"],
        "email" => $data["email"],
        "last_name" => $data["last_name"],
        "first_name" => $data["first_name"],
    ];

    insertTable("users", $user);
    toSession("user", $user);
    sendTo("user/" . $user['username']);
});

$router->map('GET', '/logout', function() {
    stopSession();
    sendTo("login");
});

###################################################################
###################################################################
###################################################################

$router->map('GET', '/user/dashboard', function() {
    allowOrDie("user");
    $user = fromSession("user");
    getPage("admin/dashboard");
});

$router->map('GET', '/user/[:username]', function($username) {
    fromSession("user");
    echo $username;
//    sendTo("login");
//    include linkPage("ching/index");
});

###################################################################
###################################################################
###################################################################
###################################################################
//prescriptionS
$router->map('GET', '/prescriptions', function() {
    $prescriptions = selectTable("patient_prescription");
    $medications = selectTable("patient_medication");
    $patients = selectTable("patients");

    include linkPage("prescriptions/index");
});

$router->map('POST', '/prescriptions/add', function() {
    $user = fromSession("user");
    $data = getPost();
    $prescription = [
        "id_patient" => $data["id_patient"],
        "id_doctor" => $user["id"],
//        "status" => $data["status"],
        "remarks" => $data["remarks"],
    ];
    $patients = selectTable("patients");
    $prescription["id"] = insertTable("patient_prescription", $prescription);
    if (isset($data["medication"])) {
        for ($i = 0; $i < count($data["medication"]); $i++) {
            $medication = [
                "id_patient_prescription" => $prescription["id"],
                "medication" => $data["medication"][$i],
                "frequency" => $data["frequency"][$i],
                "dose" => $data["dose"][$i],
                "route" => $data["route"][$i],
            ];
            $medication["id"] = insertTable("patient_medication", $medication);
        }
    }



    sendTo("back");
});

$router->map('POST', '/prescriptions/edit/[i:id]', function($id) {
    
});



$router->map('GET', '/prescriptions/delete/[i:id]', function($id) {
    $prescriptions = deleteTable("patient_prescription", $id);
    $medication = deleteTable("patient_prescription", ["id_patient_prescription" => $id]);
    sendTo("back");
});



//DOCUMENTS
$router->map('GET', '/documents', function() {
    $patients = selectTable("patients");
    $patients = selectTable("patients");
    $documents = selectTable("document");
    if ($documents == null) {
        $documents = [];
    }
    include linkPage("documents/index");
});

$router->map('POST', '/documents/add', function() {
    $user = fromSession("user");
    $patients = selectTable("patients");
    $data = getPost();
    $document = [
        "id_patient" => $data["id_patient"],
        "name" => $data["name"],
        "type" => $data["type"],
        "status" => $data["status"],
        "date_issued" => $data["date_issued"],
        "date_paid" => $data["date_paid"],
        "payment_code" => $data["payment_code"],
    ];
    $document["id"] = insertTable("document", $document);
    $file = fileUpload("files/", $_FILES["file"], $document["id"]);
    updateTable("document", ["file" => $file], $document["id"]);
    sendTo("back");
});

$router->map('POST', '/documents/edit/[i:id]', function($id) {
    $user = fromSession("user");
    $patients = selectTable("patients");
    $data = getPost();
    $document = [
        "id_patient" => $data["id_patient"],
        "name" => $data["name"],
        "type" => $data["type"],
        "status" => $data["status"],
        "date_issued" => $data["date_issued"],
        "date_paid" => $data["date_paid"],
        "payment_code" => $data["payment_code"],
    ];
    if ($data["file"] != null) {
        $document["file"] = fileUpload("files/", $_FILES["file"], $id);
    }
    updateTable("document", $document, $id);
    sendTo("back");
});

$router->map('POST', '/documents/request/[i:id]', function($id) {
    $user = fromSession("user");
    $users = selectTable("users");
    $data = getPost();
    $d = getTable("document", $id);
    if ($d["payment_code"] == $data["payment_code"]) {
        updateTable("document", ["status" => "paid"], $id);
    }
    sendTo("back");
});

$router->map('GET', '/documents/delete/[i:id]', function($id) {
    $documents = deleteTable("document", $id);
    sendTo("back");
});



//FORUMS
$router->map('GET', '/forums', function() {
    $forum_questions = selectTable("forum_question");
    $forum_replies = selectTable("forum_reply");

    include linkPage("forums/index");
});

$router->map('POST', '/forums/add', function() {
    $user = fromSession("user");
    $data = getPost();

    $forum = [
        "id_user" => $user["id"],
        "question" => $data["question"],
        "type" => $data["type"],
        "status" => "open",
    ];

    $forum["id"] = insertTable("forum_question", $forum);

    sendTo("forums");
});

$router->map('POST', '/forums/reply/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();

    $forum = [
        "id_question" => $id,
        "id_user" => $user["id"],
        "reply" => $data["reply"],
        "type" => $data["type"],
        "status" => "open",
    ];
    $forum["id"] = insertTable("forum_reply", $forum);
    sendTo("forums");
});

$router->map('POST', '/forums/edit/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();

    $forum = [
        "question" => $data["question"],
        "type" => $data["type"],
        "status" => $data["type"],
    ];

    updateTable("forum_quiestion", $forum, $id);
    sendTo("forums");
});

$router->map('GET', '/forums/question/vote/[*:vote]/[i:id]', function($vote, $id) {
    $user = fromSession("user");
    $forum = getTable("forum_question", $id);
    if ($vote == "up") {
        $forum = ["vote_up" => $forum["vote_up"] . "," . $user["id"]];
    } else {
        $forum = ["vote_down" => $forum["vote_down"] . "," . $user["id"]];
    }
    updateTable("forum_question", $forum, $id);
    sendTo("forums");
});
$router->map('GET', '/forums/reply/vote/[*:vote]/[i:id]', function($vote, $id) {
    $user = fromSession("user");
    $forum = getTable("forum_reply", $id);

    if ($vote == "up") {
        $forum = ["vote_up" => $forum["vote_up"] . "," . $user["id"]];
    } else {
        $forum = ["vote_down" => $forum["vote_down"] . "," . $user["id"]];
    }
    updateTable("forum_reply", $forum, $id);
    sendTo("forums");
});

$router->map('POST', '/forums/reply/add/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();
    $reply = [
        "id_question" => $id,
        "id_user" => $user["id"],
        "reply" => $data["reply"],
    ];
    insertTable("forum_reply", $reply, $id);
    sendTo("forums");
});

$router->map('GET', '/forums/delete/[i:id]', function($id) {
    $forums = deleteTable("forum", $id);
    sendTo("forums");
});








//EMPLOYEES
$router->map('GET', '/employees', function() {
    $employees = selectTable("users", ["type[!]" => "patient"]);
    if ($employees == null) {
        $employees = [];
    }
    include linkPage("employees/index");
});

$router->map('GET', '/employees/add', function() {
    $employees = selectTable("users");

    include linkPage("employees/add");
});

$router->map('POST', '/employees/add', function() {
    $user = fromSession("user");
    $data = getPost();
    $employee = [
        "last_name" => $data["last_name"],
        "first_name" => $data["first_name"],
        "username" => $data["email"],
        "email" => $data["email"],
        "password" => $data["password"],
        "contact_number" => $data["contact_number"],
        "birthday" => $data["birthday"],
        "gender" => $data["gender"],
        "type" => $data["type"],
    ];

    $employee["id"] = insertTable("users", $employee);
    if ($_FILES["picture"]["name"] != "") {
        $file = fileUpload("pictures/", $_FILES["picture"], 0);
        updateTable("users", ["picture" => $file], $employee["id"]);
    }
    if ($_FILES["resume"]["name"] != "") {
        $file = fileUpload("resumes/", $_FILES["resume"], 0);
        updateTable("users", ["resume" => $file], $employee["id"]);
    }
    sendTo("employees");
});

$router->map('POST', '/employees/edit/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();
    $employee = [
        "last_name" => $data["last_name"],
        "first_name" => $data["first_name"],
//        "middle_name" => $data["middle_name"],
        "email" => $data["email"],
        "password" => $data["password"],
        "contact_number" => $data["contact_number"],
        "birthday" => $data["birthday"],
        "gender" => $data["gender"],
        "type" => $data["type"],
    ];

    updateTable("users", $employee, $id);
    sendTo("back");
});



$router->map('GET', '/employees/delete/[i:id]', function($id) {
    $employees = deleteTable("users", $id);
    sendTo("employees");
});





//SCHEDULES
$router->map('GET', '/rounds', function() {
    $user = fromSession("user");
    $users = selectTable("users");
    if ($user["type"] == "doctor") {
        $rounds = selectTable("room_assignments", ["id_doctor" => $user["id"]]);
    } else if ($user["type"] == "nurse") {
        $rounds = selectTable("room_assignments", ["id_nurse" => $user["id"]]);
    }
    $vitals = selectTable("patient_vitals", ["ORDER" => "id DESC"]);
    $patients = selectTable("patients");
    include linkPage("rounds/index");
});

$router->map('POST', '/rounds/add', function() {
    $user = fromSession("user");
    $users = selectTable("users");
    $data = getPost();

    $round = [
        "date_start" => $data["date_start"],
        "date_end" => $data["date_end"],
        "type" => $data["type"],
        "id_patient" => $user["id"],
        "id_doctor" => $data["id_doctor"],
        "id_nurse" => $data["id_nurse"],
        "id_room" => $data["id_room"],
        "id_bed" => $data["id_bed"],
        "triange" => $data["triange"],
        "remarks" => $data["remarks"],
        "status" => "pending",
        "triange" => $data["triange"],
    ];

    $round["id"] = insertTable("room_assignments", $round);

    sendTo("rounds");
});

$router->map('POST', '/rounds/edit/[i:id]', function($id) {
    $user = fromSession("user");
    $users = selectTable("users");
    $data = getPost();

    $round = [
        "date_start" => $data["date_start"],
        "date_end" => $data["date_end"],
        "type" => $data["type"],
        "id_patient" => $user["id"],
        "id_doctor" => $data["id_doctor"],
        "id_nurse" => $data["id_nurse"],
        "id_room" => $data["id_room"],
        "id_bed" => $data["id_bed"],
        "remarks" => $data["remarks"],
        "triange" => $data["triange"],
        "remarks" => $data["remarks"],
        "status" => "pending",
        "triange" => $data["triange"],
    ];

    $round["id"] = updateTable("room_assignments", $round, $id);

    sendTo("back");
});

$router->map('POST', '/rounds/accept/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();

    $round = [
        "id_doctor" => $user["id"],
        "status" => "accept",
    ];

    $round["id"] = updateTable("room_assignments", $round, $id);

    sendTo("rounds");
});

$router->map('GET', '/rounds/delete/[i:id]', function($id) {
    $rounds = deleteTable("room_assignments", $id);
    sendTo("rounds");
});



//SCHEDULES
$router->map('GET', '/schedules', function() {
    $user = fromSession("user");
    $users = selectTable("users");
    if ($user["type"] == "patient") {
        $schedules = selectTable("room_assignments", ["id_patient" => $user["id"]]);
    } else {
        $patients = selectTable("patients");
        $schedules = selectTable("room_assignments");
    }
    include linkPage("schedules/index");
});

$router->map('POST', '/schedules/add', function() {
    $user = fromSession("user");
    $data = getPost();

    $schedule = [
        "date_start" => $data["date_start"],
        "date_end" => $data["date_end"],
        "type" => $data["type"],
        "id_patient" => $data["id_patient"],
        "id_doctor" => $data["id_doctor"],
        "id_nurse" => $data["id_nurse"],
        "id_room" => $data["id_room"],
        "id_bed" => $data["id_bed"],
        "triange" => $data["triange"],
        "remarks" => $data["remarks"],
        "status" => "pending",
        "triange" => $data["triange"],
    ];

    $schedule["id"] = insertTable("room_assignments");

    sendTo("schedules");
});

$router->map('POST', '/schedules/add/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();
    if ($data["type"] == "consultation") {
        $date_end = date("Y-m-d H:i:s", strtotime('+30 minutes', strtotime($data["date_start"])));
    } else {
        $date_end = date("Y-m-d H:i:s", strtotime('+60 minutes', strtotime($data["date_start"])));
    }
    $schedule = [
        "date_start" => $data["date_start"],
        "date_end" => $date_end,
        "type" => $data["type"],
        "id_patient" => $id,
        "id_doctor" => $data["id_doctor"],
        "id_nurse" => $data["id_nurse"],
        "id_room" => $data["id_room"],
        "id_bed" => $data["id_bed"],
        "triange" => $data["triange"],
        "remarks" => $data["remarks"],
        "status" => $data["status"],
        "triange" => $data["triange"],
    ];

    $schedule["id"] = insertTable("room_assignments", $schedule);

    sendTo("patients/$id");
});
$router->map('POST', '/schedules/add/[i:id]/self', function($id) {
    $user = fromSession("user");
    $data = getPost();
    if ($data["type"] == "consultation") {
        $date_end = date("Y-m-d H:i:s", strtotime('+30 minutes', strtotime($data["date_start"])));
    } else {
        $date_end = date("Y-m-d H:i:s", strtotime('+60 minutes', strtotime($data["date_start"])));
    }

    $schedule = [
        "id_patient" => $id,
        "date_start" => $data["date_start"],
        "date_end" => $date_end,
        "type" => $data["type"],
        "remarks" => $data["remarks"],
        "status" => "pending",
    ];

    $schedule["id"] = insertTable("room_assignments", $schedule);

    sendTo("patients/$id");
});

$router->map('POST', '/schedules/edit/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();
    if ($data["type"] == "consultation") {
        $date_end = date("Y-m-d H:i:s", strtotime('+30 minutes', strtotime($data["date_start"])));
    } else {
        $date_end = date("Y-m-d H:i:s", strtotime('+60 minutes', strtotime($data["date_start"])));
    }
    $schedule = [
        "date_start" => $data["date_start"],
        "date_end" => $date_end,
        "type" => $data["type"],
        "id_doctor" => $data["id_doctor"],
        "id_nurse" => $data["id_nurse"],
        "id_room" => $data["id_room"],
        "id_bed" => $data["id_bed"],
        "remarks" => $data["remarks"],
        "triange" => $data["triange"],
        "remarks" => $data["remarks"],
        "status" => $data["status"],
        "triange" => $data["triange"],
    ];

    $schedule["id"] = updateTable("room_assignments", $schedule, $id);

    sendTo("back");
});

$router->map('POST', '/schedules/accept/[i:id]', function($id) {
    $user = fromSession("user");
    $data = getPost();

    $schedule = [
        "id_doctor" => $user["id"],
        "status" => "accept",
    ];

    $schedule["id"] = updateTable("room_assignments", $schedule, $id);

    sendTo("schedules");
});

$router->map('GET', '/schedules/delete/[i:id]', function($id) {
    $schedules = deleteTable("room_assignments", $id);
    sendTo("schedules");
});



//PATIENTS
$router->map('GET', '/patients', function() {
    $patients = selectTable("patients");
    include linkPage("patients/index");
});
$router->map('GET', '/patients/add', function() {
    include linkPage("patients/add");
});
$router->map('POST', '/patients/add', function() {
    $data = getPost();
    $patient = [
        "last_name" => $data["last_name"],
        "first_name" => $data["first_name"],
        "middle_name" => $data["middle_name"],
        "email" => $data["email"],
        "username" => $data["email"],
        "password" => "root",
        "contact_number" => $data["contact_number"],
        "blood_type" => $data["blood_type"],
        "birthday" => $data["birthday"],
        "gender" => $data["gender"],
        "occupation" => $data["occupation"],
        "address" => $data["address"],
        "patient_number" => $data["patient_number"] ? $data["patient_number"] : "",
    ];
    $puser = [
        "patient_number" => $data["patient_number"] ? $data["patient_number"] : "",
        "last_name" => $data["last_name"],
        "first_name" => $data["first_name"],
        "email" => $data["email"],
        "username" => $data["email"],
        "password" => "root",
        "birthday" => $data["birthday"],
        "gender" => $data["gender"],
        "type" => "patient",
    ];

    $patient_demographics = [
        "weight" => $data["weight"],
        "height" => $data["height"],
        "temperature" => $data["temperature"],
        "pulse" => $data["pulse"],
        "systolic" => $data["systolic"],
        "diastolic" => $data["diastolic"],
        "respiration" => $data["respiration"],
        "companion_name" => $data["companion_name"],
        "companion_complaint" => $data["companion_complaint"],
    ];

    insertTable("users", $puser);

    $patient["id"] = insertTable("patients", $patient);
    $patient_demographics["id_patient"] = $patient["id"];
    $patient_demographics["id"] = insertTable("patient_demographics", $patient_demographics);

    if ($_FILES["picture"]["name"] != "") {
        $file = fileUpload("pictures/", $_FILES["picture"], 0);
        updateTable("users", ["picture" => $file], $patient);
    }

    sendTo("patients");
});

$router->map('POST', '/patients/[i:id]/demographics/add', function($id) {
    $data = getPost();


    $patient_demographics = [
        "weight" => $data["weight"],
        "height" => $data["height"],
        "temperature" => $data["temperature"],
        "pulse" => $data["pulse"],
        "systolic" => $data["systolic"],
        "diastolic" => $data["diastolic"],
        "respiration" => $data["respiration"],
        "companion_name" => $data["companion_name"],
        "companion_complaint" => $data["companion_complaint"],
    ];

    $patient_demographics["id_patient"] = $id;
    $patient_demographics["id"] = insertTable("patient_demographics", $patient_demographics);

    sendTo("patients/$id");
});

$router->map('POST', '/patients/[i:id]/vitals/add', function($id) {
    $user = fromSession("user");
    $data = getPost();
    $patient_vitals = [
        "remarks" => $data["remarks"],
        "id_patient" => $id,
        "id_doctor" => $user["id"],
    ];
    $patient_vitals["id"] = insertTable("patient_vitals", $patient_vitals);
    sendTo("back");
});


$router->map('GET', '/patients/[i:id]', function($id) {
    $users = selectTable("users");
    $patient = getTable("patients", $id);
    $schedules = selectTable("room_assignments", ["id_patient" => $id]);
    $documents = selectTable("document", ["id_patient" => $id]);
    $patient_demographics = selectTable("patient_demographics", ["id_patient" => $id]);
    $prescriptions = selectTable("patient_prescription", ["id_patient" => $id]);
    $medications = selectTable("patient_medication");
    include linkPage("patients/show");
});
$router->map('POST', '/patients/edit/[i:id]', function($id) {
    $data = getPost();
    $patient = updateTable("patients", $data, $id);
    sendTo("patients/$id");
});
$router->map('GET', '/patients/delete/[i:id]', function($id) {
    $patient = deleteTable("patients", $id);
    sendTo("patients");
});
$router->map('GET', '/patients/demographics/delete/[i:id]', function($id) {
    $patient = deleteTable("patient_demographicss", $id);
    sendTo("back");
});

//PATIENTS
###################################################################
###################################################################
###################################################################
###################################################################

$router->map('GET', '/seed', function() {
    echo "db seeding started";
    $users = [
        [
            "id" => "1",
            "last_name" => "Manisa",
            "first_name" => "Marcota",
            "email" => "markryan@gmail.com",
            "username" => "admin",
            "password" => "root",
            "birthday" => "1860-02-02",
            "gender" => "male",
            "type" => "admin"
        //admin, doctor, nurse, medtech, 
        ],
        [
            "id" => "2",
            "last_name" => "Chang",
            "first_name" => "Sueu",
            "email" => "markryan@gmail.com",
            "username" => "doctor",
            "password" => "root",
            "birthday" => "1860-02-02",
            "gender" => "male",
            "type" => "doctor"
        //admin, doctor, nurse, medtech, 
        ],
        [
            "id" => "3",
            "last_name" => "Rhianna",
            "first_name" => "Mana",
            "email" => "markryan@gmail.com",
            "username" => "nurse",
            "password" => "root",
            "birthday" => "1860-02-02",
            "gender" => "male",
            "type" => "nurse"
        //admin, doctor, nurse, medtech, 
        ],
        [
            "id" => "4",
            "last_name" => "Ryana",
            "first_name" => "Marky",
            "email" => "markryan@gmail.com",
            "username" => "medtech",
            "birthday" => "1860-02-02",
            "password" => "root",
            "gender" => "male",
            "type" => "medtech"
        //admin, doctor, nurse, medtech, 
        ],
    ];


    foreach ($users as $p) {
        insertTable("users", $p);
    }
    echo "db seeded";
});

function loopDB($table, $data) {
    
}

###################################################################
###################################################################
###################################################################
###################################################################
###################################################################

$match = $router->match();

if ($match && is_callable($match['target'])) {
    call_user_func_array($match['target'], $match['params']);
} else if ($match) {
    require $match['target'];
} else {
    header("HTTP/1.0 404 Not Found");
    require '404.php';
}
  