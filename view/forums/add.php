<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("Add New Patient Record") ?>
                    <!--<small></small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Patients</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <form action="<?= linkTo("patients/add") ?>" method="POST">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">First Name</label>
                                                <input type="text" name="first_name" class="form-control" id="first_name" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Middle Name</label>
                                                <input type="text" name="middle_name" class="form-control" id="middle_name" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Last Name</label>
                                                <input type="text" name="last_name" class="form-control" id="last_name" placeholder="">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Birthdate</label>
                                                <input type="date" name="birthday" class="form-control" id="birthday" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Gender</label>
                                                <select class="form-control" name="gender">
                                                    <?php foreach (["male", "female"] as $g) { ?>
                                                        <option><?= $g ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Blood Type</label>
                                                <select class="form-control" name="blood_type">
                                                    <?php foreach (["A+", "A-", "B+", "B-", "O", "AB+", "AB-"] as $g) { ?>
                                                        <option><?= $g ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Contact Number</label>
                                                <input type="text" name="contact_number" class="form-control" id="contact_number" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input type="email" name="email" class="form-control" id="email" placeholder="">
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-md-4">
                                                                                </div>-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Address</label>
                                                <textarea class="form-control" name="address" rows="5" cols="20"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nationality</label>
                                                <input type="text" name="nationality" class="form-control" id="nationality" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Occupation</label>
                                                <input type="text" name="occupation" class="form-control" id="occupation" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Religion</label>
                                                <input type="text" name="religion" class="form-control" id="religion" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Companion Name</label>
                                                <input type="text" name="companion_name" class="form-control" id="companion_name" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Companion Complaint</label>
                                                <input type="text" name="companion_complaint" class="form-control" id="companion_complaint" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                    <hr/>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Weight</label>
                                                <input type="text" name="weight" class="form-control" id="weight" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Height</label>
                                                <input type="text" name="height" class="form-control" id="height" placeholder="">
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Pulse</label>
                                                <input type="text" name="pulse" class="form-control" id="pulse" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Respiration</label>
                                                <input type="text" name="respiration" class="form-control" id="respiration" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Systolic</label>
                                                <input type="text" name="systolic" class="form-control" id="sysctolic" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Diastolic</label>
                                                <input type="text" name="diastolic" class="form-control" id="diastolic" placeholder="">
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Temperature</label>
                                                <input type="text" name="temperature" class="form-control" id="temperature" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="clearfix ">
                                        <div class="btn-group pull-right ">
                                            <button type="reset" class=" btn btn-default"><i class="fa fa-eraser"></i> Clear Fields</button>
                                            <button type="submit" class=" btn btn-primary"><i class="fa fa-check-circle"></i> Submit New Patient Record</button>
                                        </div>
                                    </div>

                                </form>
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<script>
    $("table").dataTable();

//    $("input [aria-controls='DataTables_Table_0']").val();

</script>

<?php
include linkPage("template/footer_admin");
?>