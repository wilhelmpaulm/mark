<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("polls") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Patients</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="dtable table table-bordered table-responsive table-condensed">
                                            <thead>
                                                <tr>
                                                    <!--<th>ID</th>-->
                                                    <th width="5">Date</th>
                                                    <th>Question</th>
                                                    <th width="5">User</th>
                                                    <th width="20%" >Votes</th>

                                                    <th width="5">
                                                        <a href="#" data-toggle="modal" data-target="#addQuestion" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Question</a>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($forum_questions as $p) { ?>
                                                    <tr>
                                                        <!--<td><?= $p["id"] ?></td>-->
                                                        <td >
                                                            <span class="label label-default">
                                                                <?= $p["date_created"] ?>
                                                            </span>
                                                        </td>
                                                        <td><?= $p["question"] ?></td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $p["id_user"] ?>
                                                            </span>
                                                        </td>
                                                        <td>



                                                            <span class="btn-group btn-group-justified" style="width: 12em">

                                                                <?php
                                                                $aup = explode(",", $p["vote_up"]);
                                                                $cup = count($aup) - 1;

                                                                $adown = explode(",", $p["vote_down"]);
                                                                $cdown = count($adown) - 1;

                                                                $tup = "";
                                                                $tdown = "";
                                                                if (in_array($user["id"], $aup)) {
                                                                    $tup = "disabled";
                                                                    $tdown = "disabled";
                                                                }
                                                                if (in_array($user["id"], $adown)) {
                                                                    $tup = "disabled";
                                                                    $tdown = "disabled";
                                                                }
                                                                ?>
                                                                <a href="<?= linkTo("forums/question/vote/up/" . $p["id"]) ?>" class="btn btn-success btn-xs <?= $tup ?>"><i class="fa fa-thumbs-o-up"></i>  <?= $cup ?></a>
                                                                <a href="<?= linkTo("forums/question/vote/down/" . $p["id"]) ?>" class="btn btn-danger btn-xs <?= $tdown ?>"><i class="fa fa-thumbs-o-down"></i>  <?= $cdown ?></a>
                                                            </span>
                                                        </td>
                                                        <td>

                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                <a href="#" data-toggle="modal" data-target="#question_<?= $p["id"] ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                <a href="<?= linkTo("forums/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>

                                    </div><!-- /.box -->
                                </div><!-- /.box -->
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>




<div class="modal fade" id="addQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Question</h4>
            </div>
            <form action="<?= linkTo("forums/add") ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Question</label>
                                <textarea class="form-control" name="question" rows="4" cols="20"></textarea>
                            </div>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-8">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type</label>
                                <select class="form-control" name="type">
                                    <?php foreach (["high", "mid", "low"] as $t) { ?>
                                        <option value="<?= $t ?>" ><?= $t ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php foreach ($forum_questions as $fq) { ?>

    <div class="modal fade" id="question_<?= $fq["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-xs-12">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Show Question</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Question</label>
                                            <textarea class="form-control" readonly="" name="question" rows="5" cols="20"><?= $fq["question"] ?></textarea>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">User</label>
                                            <input readonly="" class="form-control" value="<?= $fq["id_user"] ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Type</label>
                                            <input readonly="" class="form-control" value="<?= $fq["type"] ?>">
                                        </div>

                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
                                            <input readonly="" class="form-control" value="<?= $fq["status"] ?>">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th >User</th>
                                            <th>Reply</th>
                                            <th width="20">Votes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($forum_replies as $fr) {
                                            if ($fq["id"] == $fr["id_question"]) {
                                                ?>
                                                <tr>
                                                    <td width="5">
                                                        <span class="label label-default">
                                                            <?= $fr["id_user"] ?>
                                                        </span>
                                                    </td>
                                                    <td>

                                                        <?= $fr["reply"] ?>
                                                    </td>
                                                    <td width="20%">



                                                        <span class="btn-group btn-group-justified" style="width: 12em">

                                                            <?php
                                                            $aup = explode(",", $fr["vote_up"]);
                                                            $cup = count($aup) - 1;

                                                            $adown = explode(",", $fr["vote_down"]);
                                                            $cdown = count($adown) - 1;

                                                            $tup = "";
                                                            $tdown = "";
                                                            if (in_array($user["id"], $aup)) {
                                                                $tup = "disabled";
                                                                $tdown = "disabled";
                                                            }
                                                            if (in_array($user["id"], $adown)) {
                                                                $tup = "disabled";
                                                                $tdown = "disabled";
                                                            }
                                                            ?>
                                                            <a href="<?= linkTo("forums/reply/vote/up/" . $fr["id"]) ?>" class="btn btn-success btn-xs <?= $tup ?>"><i class="fa fa-thumbs-o-up"></i>  <?= $cup ?></a>
                                                            <a href="<?= linkTo("forums/reply/vote/down/" . $fr["id"]) ?>" class="btn btn-danger btn-xs <?= $tdown ?>"><i class="fa fa-thumbs-o-down"></i>  <?= $cdown ?></a>
                                                        </span>
                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <form action="<?= linkTo("forums/reply/add/" . $fq["id"]) ?>" method="POST">
                                        <tr>
                                            <td colspan="2"><input name="reply" class="form-control"></td>
                                            <td width="5">
                                                <button class="btn btn-xs btn-block btn-primary"><i class="fa fa-check-circle"></i> Reply</button> 
                                            </td>
                                        </tr>
                                    </form>
                                    </tbody>
                                </table>
                                <!--                                <br/>
                                                                <hr/>
                                                                <table class="table table-bordered table-condensed">
                                                                    <tbody>
                                                                    <form action="<?= linkTo("forums/reply/add/" . $fq["id"]) ?>" method="POST">
                                                                        <tr>
                                                                            <td colspan="2"><input name="reply" class="form-control"></td>
                                                                            <td width="5">
                                                                                <button class="btn btn-xs btn-primary"><i class="fa fa-check-circle"></i> Reply</button> 
                                                                            </td>
                                                                        </tr>
                                                                    </form>
                                                                    </tbody>
                                                                </table>-->






                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
    <!--                        <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>-->
                    </div>

                </div>
            </div>
        </div>
    </div>



<?php } ?>








<script>
    $(".dtable").dataTable();

//    $("input [aria-controls='DataTables_Table_0']").val();

</script>

<?php
include linkPage("template/footer_admin");
?>