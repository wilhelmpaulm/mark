<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("SCHEDULES") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Schedules</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr class="">
                                                    <th>Patient</th>
                                                    <th>Status</th>
                                                    <th>Date Set</th>
                                                    <th>Date Start</th>
                                                    <th>Date End</th>
                                                    <th>Doctor</th>
                                                    <th>Nurse</th>
                                                    <th>Type</th>
                                                    <th>Room</th>
                                                    <th>Bed</th>
                                                    <th>Triange</th>
                                                    <th width="5">
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="hidden btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addSchedules">
                                                            <i class="fa fa-plus-circle"></i> Add Appointment
                                                        </button>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($schedules as $s) { ?>
                                                    <?php
                                                    $doctor = [];
                                                    $nurse = [];
                                                    $patient = [];
                                                    foreach ($users as $u) {
                                                        if ($s["id_doctor"] == $u["id"]) {
                                                            $doctor = $u;
                                                        }
                                                        if ($s["id_patient"] == $u["id"]) {
                                                            $patient = $u;
                                                        }
                                                        if ($s["id_nurse"] == $u["id"]) {
                                                            $nurse = $u;
                                                        }
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?= $patient["last_name"] ?>, <?= $patient["first_name"] ?></td>

                                                        <td>
                                                            <span class="label label-info">
                                                                <?= $s["status"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_created"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_start"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_end"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <?php if ($doctor): ?>
                                                                <?= $doctor["last_name"] ?>, <?= $doctor["first_name"] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($nurse): ?>
                                                                <?= $nurse["last_name"] ?>, <?= $nurse["first_name"] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <span class="label bg-teal">
                                                                <?= $s["type"] ?>
                                                            </span>
                                                        </td>
                                                        <td><?= $s["id_room"] ?></td>
                                                        <td><?= $s["id_bed"] ?></td>
                                                        <td><?= $s["triange"] ?></td>
                                                        <td>
                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                <?php if (!in_array(getUser("type"), ["patient"])): ?>
                                                                    <a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#schedules_<?= $s["id"] ?>"><i class="fa fa-circle-o"></i> View</a>
                                                                <?php endif; ?>
                                                                <a href="<?= linkTo("schedules/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>


                                    </div><!-- /.box -->
                                </div><!-- /.box --> 
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>


<?php foreach ($schedules as $s) { ?>
    <?php
    $setView = "";
    if (getUser("type") == "admin") {
        $setView = "readonly=''";
    }
    ?>

    <div class="modal fade" id="schedules_<?= $s["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-xs-12" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Schedule #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("schedules/edit/" . $s["id"]) ?>" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Doctor</label>
                                        <select class="form-control" name="id_doctor" <?= $setView?>>
                                            <?php if ($doctor): ?>
                                                <option value="<?= $s["id_doctor"] ?>" ><?= $doctor["last_name"] ?>, <?= $doctor["first_name"] ?></option>
                                            <?php endif; ?>

                                            <?php foreach ($users as $t) { ?>
                                                <?php if ($t["type"] == "doctor") { ?>
                                                    <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>                              

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nurse</label>
                                        <select class="form-control" name="id_nurse" <?= $setView?>>

                                            <?php if ($nurse): ?>
                                                <option value="<?= $s["id_nurse"] ?>" ><?= $nurse["last_name"] ?>, <?= $nurse["first_name"] ?></option>
                                            <?php endif; ?>
                                            <?php foreach ($users as $t) { ?>
                                                <?php if ($t["type"] == "nurse") { ?>
                                                    <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>                                      
                                </div>

                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Room</label>
                                        <select class="form-control" name="id_room" <?= $setView?>>
                                            <option value="<?= $s["id_room"] ?>" ><?= $s["id_room"] ?></option>
                                            <?php foreach ([101, 102, 103, 104, 105, 106] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Bed</label>
                                        <select class="form-control" name="id_bed" <?= $setView?>>
                                            <option value="<?= $s["id_bed"] ?>" ><?= $s["id_bed"] ?></option>
                                            <?php foreach ([1, 2, 3, 4, 5, 6] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                              
                                </div>

                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Triange</label>
                                        <select class="form-control" name="triange" <?= $setView?>>
                                            <?php foreach ([1] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type</label>
                                        <select class="form-control" name="type" <?= $setView?>>
                                            <?php foreach (["consultation", "examination"] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Status</label>
                                        <select class="form-control" name="status">
                                            <?php
                                            $statuses = [];
                                            if ($s["status"] == "pending") {
                                                $statuses = ["confirmed", "rescheduled", "declined"];
                                            } else if ($s["status"] == "confirmed") {
                                                $statuses = ["accepted", "declined"];
                                            }
                                            ?>
                                            <?php foreach ($statuses as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Start</label>
                                        <input type="datetime" <?= $setView?> name="date_start" class="form-control" id="date_start" value="<?= $s["date_start"] ?>">
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="exampleInputEmail1">Date End</label>
                                        <input type="datetime" <?= $setView?> name="date_end" class="form-control" id="date_end" value="<?= $s["date_end"] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Remarks</label>
                                        <textarea  <?= $setView?> class="form-control" name="remarks" rows="4" cols="20"><?= $s["remarks"] ?></textarea>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="modal-footer">

                                                                                                                                                                    <!--<a href="<?= linkTo("schedules/delete/" . $s["id"]) ?>" class="btn btn-success "><i class="fa fa-plus-circle"></i> Accept</a>-->
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<div class="modal fade" id="addSchedules" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Schedule</h4>
            </div>
            <form action="<?= linkTo("schedules/add") ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="exampleInputEmail1">Patient</label>
                            <?php if (fromSession("user")["type"] != "patient"): ?>
                                <select class="form-control" name="id_patient">
                                    <?php foreach ($patients as $t) { ?>
                                        <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                    <?php } ?>
                                </select>
                            <?php else: ?>
                                <input type="hidden" class="form-control" name="id_patient" value="<?= fromSession("puser")["id"] ?>">
                                <input type="text" readonly="" class="form-control" value="<?= fromSession("puser")["last_name"] ?>, <?= fromSession("puser")["first_name"] ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="exampleInputEmail1">Doctor</label>
                            <select class="form-control" name="id_doctor">
                                <?php foreach ($patients as $t) { ?>
                                    <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="exampleInputEmail1">Nurse</label>
                            <select class="form-control" name="id_nurse">
                                <?php foreach ($users as $t) { ?>
                                    <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type</label>
                                <select class="form-control" name="type">
                                    <?php foreach (["consultation", "examination"] as $t) { ?>
                                        <option value="<?= $t ?>" ><?= $t ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Remarks</label>
                                <textarea class="form-control" name="remarks" rows="4" cols="20"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date Start</label>
                                <input type="datetime" name="date_start" class="form-control" id="date_start" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date End</label>
                                <input type="datetime" name="date_end" class="form-control" id="date_end" placeholder="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>











<script>
    $("table").dataTable();
    $('[type=datetime]').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script>

<?php
include linkPage("template/footer_admin");
?>