<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("DOCUMENTS") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Documents</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr class="">
                                                    <th>Status</th>
                                                    <th>Patient</th>
                                                    <th>Name</th>
                                                    <th>File</th>
                                                    <th>Date Created</th>
                                                    <th>Issue Date</th>
                                                    <th>Date Paid</th>
                                                    <th>Payment Code</th>
                                                    <th>Type</th>
                                                    <th width="5">
                                                        <?php if (in_array($user["type"], ["medtech"])) { ?>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addSchedules">
                                                                <i class="fa fa-plus-circle"></i> Add New Document
                                                            </button>
                                                        <?php } ?>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($documents as $s) { ?>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-info">
                                                                <?= $s["status"] ?>
                                                            </span>
                                                        </td>
                                                        <?php
                                                        $patient = [];
                                                        foreach ($patients as $p) {
                                                            if ($s["id_patient"] == $p["id"]) {
                                                                $patient = $p;
                                                            }
                                                        }
                                                        ?>
                                                        <td><?= $patient["last_name"] ?>, <?= $patient["first_name"] ?></td>
                                                        <td><?= $s["name"] ?></td>

                                                        <td>
                                                            <span class="label bg-wisteria">
                                                                <?= $s["file"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_created"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_issued"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_paid"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["payment_code"] ?>
                                                            </span>
                                                        </td>

                                                        <td>
                                                            <span class="label bg-teal">
                                                                <?= $s["type"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                <?php if ($s["status"] == "pending") { ?>
                                                                    <a href="" class="btn btn-warning btn-xs btn-block" data-toggle="modal" data-target="#documents_<?= $s["id"] ?>_request"><i class="fa fa-circle"></i> Request</a>
                                                                <?php } else { ?>
                                                                    <a href="<?= linkPublic("files/{$s["file"]}") ?>" class="btn btn-default btn-xs btn-block" target="__blank"><i class="fa fa-circle-o"></i> Download</a>
                                                                            <!--<a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#documents_<?= $s["id"] ?>"><i class="fa fa-circle-o"></i> View</a>-->
                                                                <?php } ?>
                                                                <?php if (getUser("type") == "admin" || getUser("type") == "medtech") { ?>
                                                                    <a href="<?= linkTo("documents/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                                <?php } ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>


                                    </div><!-- /.box -->
                                </div><!-- /.box --> 
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>


<?php foreach ($documents as $s) { ?>

    <div class="modal fade" id="documents_<?= $s["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-xs-12" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Schedule #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("documents/edit/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Patient</label>
                                    <select class="form-control" name="id_patient">
                                        <?php
                                        $patient = [];
                                        foreach ($patients as $u) {
                                            if ($s["id_patient"] == $u["id"]) {
                                                $patient = $u;
                                            }
                                        }
                                        ?>  
                                        <option value="<?= $s["id_patient"] ?>" ><?= $patient["last_name"] . ", " . $patient["first_name"] ?></option>
                                        <?php foreach ($patients as $u) { ?>
                                            <option value="<?= $u["id"] ?>" ><?= $u["last_name"] ?>, <?= $u["first_name"] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="<?= $s["status"] ?>" ><?= $s["status"] ?></option>
                                        <?php foreach (["paid", "pending"] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Original File</label>
                                        <input type="text" readonly="" class="form-control" value="<?= $s["file"] ?>" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type</label>
                                        <select class="form-control" name="type">
                                            <option value="<?= $s["type"] ?>" ><?= $s["type"] ?></option>
                                            <?php foreach (["x-ray", "form", "QR", "lambda"] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" name="name" class="form-control" value="<?= $s["name"] ?>" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">File</label>
                                        <input type="file" name="file" class="form-control" value="<?= $s["file"] ?>">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Issued</label>
                                        <input type="datetime" name="date_issued" class="form-control" value="<?= $s["date_issued"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Paid</label>
                                        <input type="datetime" name="date_paid" class="form-control" value="<?= $s["date_paid"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Payment Code</label>
                                        <input type="text" name="payment_code" class="form-control" value="<?= $s["payment_code"] ?>">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <?php if ($user["type"] != "patient"): ?>
                                <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                            <?php endif; ?>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php foreach ($documents as $s) { ?>
    <div class="modal fade" id="documents_<?= $s["id"] ?>_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-md-6 col-md-offset-3" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Schedule #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("documents/request/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Issued</label>
                                        <input type="datetime" name="date_issued" readonly="" class="form-control" value="<?= $s["date_issued"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="datetime" name="name" readonly="" class="form-control" value="<?= $s["name"] ?>"  placeholder="">
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Payment Code</label>
                                        <input type="text" name="payment_code" class="form-control" value="">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<div class="modal fade" id="addSchedules" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Document</h4>
            </div>

            <form action="<?= linkTo("documents/add") ?>" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Patient</label>
                            <select class="form-control" name="id_patient">
                                <?php foreach ($patients as $u) { ?>
                                    <option value="<?= $u["id"] ?>" ><?= $u["last_name"] ?>, <?= $u["first_name"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" name="status">
                                <?php foreach (["paid", "pending", "released"] as $t) { ?>
                                    <option value="<?= $t ?>" ><?= $t ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type</label>
                                <select class="form-control" name="type">
                                    <?php foreach (["x-ray", "form", "QR", "lambda"] as $t) { ?>
                                        <option value="<?= $t ?>" ><?= $t ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" name="name" class="form-control"placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">File</label>
                                <input type="file" name="file" class="form-control">
                                <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date Issued</label>
                                <input type="datetime" name="date_issued" class="form-control"  placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date Paid</label>
                                <input type="datetime" name="date_paid" class="form-control"  placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Payment Code</label>
                                <input type="text" name="payment_code" class="form-control">
                                <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>











<script>
    $("table").dataTable();
    $('[type=datetime]').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $("[name='status']").change(function(){
        if($(this).val() == "pending"){
            $("[name='date_paid']").parent().hide();
        }else{
            $("[name='date_paid']").parent().show();
        }
    });

</script>



<?php
include linkPage("template/footer_admin");
?>