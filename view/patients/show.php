<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-green">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Patient <?= $patient["first_name"] . " " . $patient["middle_name"] . " " . $patient["last_name"] ?>
                    <!--<small></small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span class="">General Information</span>

                                            </div> 
                                            <div class="" style="overflow: auto">

                                                <table class="table table-condensed table-bordered">
                                                    <thead>
                                                        <tr class="">
                                                            <th>Details</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $code = strtoupper(substr($patient["last_name"], 0, 3));
                                                        $code_number = $patient["id"] + 222;
                                                        for ($index = 4; $index > strlen($code_number); $index--) {
                                                            if (strlen($code_number) < 4) {
                                                                $code_number = "0" . $code_number;
                                                            }
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td>Patient Number</td>
                                                            <td><?= $patient["patient_number"] ? $patient["patient_number"] : "$code$code_number" ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Name</td>
                                                            <td><?= $patient["first_name"] . " " . $patient["middle_name"] . " " . $patient["last_name"] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gender</td>
                                                            <td><?= $patient["gender"] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Contact Number</td>
                                                            <td><?= $patient["contact_number"] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email Address</td>
                                                            <td><?= $patient["email"] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Blood Type</td>
                                                            <td><?= $patient["blood_type"] ?></td>
                                                        </tr>

                                                    </tbody>
                                                </table>


                                            </div><!-- /.box -->
                                        </div><!-- /.box -->
                                    </div><!-- /.box -->
                                </div><!-- /.box -->

                                <br/>
                                <br/>
                                <br/>




                            </div><!-- /.box -->
                            <div class="col-md-8"><!-- /.box -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span class="">Demographics</span>

                                            </div> 
                                            <div class="" style="overflow: auto">

                                                <table class="table table-condensed table-bordered">
                                                    <thead>
                                                        <tr class="">
                                                            <th>Patient Demographics</th>
                                                            <th>Weight</th>
                                                            <th>Height</th>
                                                            <th>Temperature</th>
                                                            <th>Respiration</th>
                                                            <th width="5">
                                                                <?php if (in_array($user["type"], ["doctor", "nurse"])) { ?>
                                                                    <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#myModal">
                                                                        <i class="fa fa-plus-circle"></i> Add Demographic Record
                                                                    </button>

                                                                <?php } ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($patient_demographics as $p) { ?>
                                                            <tr>
                                                                <td><?= $p["date_created"] ?></td>
                                                                <td><?= $p["weight"] ?></td>
                                                                <td><?= $p["height"] ?></td>
                                                                <td><?= $p["temperature"] ?></td>
                                                                <td><?= $p["respiration"] ?></td>
                                                                <td>
                                                                    <span class="btn-group">
                                                                        <?php if ($user["type"] != "patient") { ?>
                                                                            <a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#demographics_<?= $p["id"] ?>"><i class="fa fa-circle-o"></i> View</a>
                                                                        <?php } ?>
                                                                        <?php if ($user["type"] != "patient" && $user["type"] != "medtech") { ?>
                                                                            <a href="<?= linkTo("patients/demographics/delete/" . $p["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                                        <?php } ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>

                                            </div><!-- /.box -->
                                        </div><!-- /.box --> 
                                    </div><!-- /.box --> 
                                </div><!-- /.box --> 
                                <div class="row">
                                    <div class="col-xs-12"><div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span class="">Appointments / Schedules</span>

                                            </div> 
                                            <div class="" style="overflow: auto">

                                                <table class="table table-condensed table-bordered">
                                                    <thead>
                                                        <tr class="">
                                                            <th>Status</th>
                                                            <th>Date Set</th>
                                                            <th>Date Start</th>
                                                            <th>Date End</th>
                                                            <th>Doctor</th>
                                                            <th>Nurse</th>
                                                            <th>Room</th>
                                                            <th>Bed</th>
                                                            <th>Type</th>
                                                            <th>Triange</th>
                                                            <th width="5">
                                                                <?php if (in_array($user["type"], ["admin", "nurse", "doctor"])) : ?>
                                                                    <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addSchedule">
                                                                        <i class="fa fa-plus-circle"></i> Add Appointment
                                                                    </button>
                                                                <?php elseif (in_array($user["type"], ["patient"])) : ?>
                                                                    <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addScheduleSelf">
                                                                        <i class="fa fa-plus-circle"></i> Add Appointment
                                                                    </button>
                                                                <?php endif; ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($schedules as $s) { ?>
                                                            <tr>
                                                                <td>
                                                                    <span class="label label-info">
                                                                        <?= $s["status"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_created"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_start"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_end"] ?>
                                                                    </span>
                                                                </td>
                                                                <?php
                                                                $doctor = [];
                                                                $nurse = [];
                                                                foreach ($users as $u) {
                                                                    if ($s["id_doctor"] == $u["id"]) {
                                                                        $doctor = $u;
                                                                    }
                                                                    if ($s["id_nurse"] == $u["id"]) {
                                                                        $nurse = $u;
                                                                    }
                                                                }
                                                                ?>
                                                                <td>
                                                                    <?php if ($doctor): ?>
                                                                        <?= $doctor["last_name"] ?>, <?= $doctor["first_name"] ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td>
                                                                    <?php if ($nurse): ?>
                                                                        <?= $nurse["last_name"] ?>, <?= $nurse["first_name"] ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td><?= $s["id_room"] ?></td>
                                                                <td><?= $s["id_bed"] ?></td>
                                                                <td>
                                                                    <span class="label bg-teal">
                                                                        <?= $s["type"] ?>
                                                                    </span>
                                                                </td>
                                                                <td><?= $s["triange"] ?></td>
                                                                <td>
                                                                    <?php if ($user["type"] != "patient" && $user["type"] != "medtech" ) { ?>
                                                                        <span class="btn-group ">
                                                                            <a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#schedules_<?= $s["id"] ?>"><i class="fa fa-circle-o"></i> View</a>
                                                                            <a href="<?= linkTo("schedules/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                                        </span>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>



                                            </div> 


                                        </div> 

                                    </div><!-- /.box -->
                                </div><!-- /.box -->



                                <div class="row hidden">
                                    <div class="col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span class="">Documents</span>

                                            </div><!-- /.box -->
                                            <div class="" style="overflow: auto">

                                                <table class="table table-condensed table-bordered">
                                                    <thead>
                                                        <tr class="">
                                                            <th>Status</th>
                                                            <th>Patient</th>
                                                            <th>Name</th>
                                                            <th>File</th>
                                                            <th>Date Created</th>
                                                            <th>Issue Date</th>
                                                            <th>Date Paid</th>
                                                            <th>Payment Code</th>
                                                            <th>Type</th>
                                                            <th width="5">
                                                                <?php if (in_array($user["type"], ["medtech"])) { ?>
                                                                    <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addDocuments">
                                                                        <i class="fa fa-plus-circle"></i> Add New Document
                                                                    </button>
                                                                <?php } ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($documents as $s) { ?>
                                                            <tr>
                                                                <td>
                                                                    <span class="label label-info">
                                                                        <?= $s["status"] ?>
                                                                    </span>
                                                                </td>
                                                                <td><?= $s["id_patient"] ?></td>
                                                                <td><?= $s["name"] ?></td>

                                                                <td>
                                                                    <span class="label bg-wisteria">
                                                                        <a href="<?= linkPublic("files/{$s["file"]}") ?>" class="c-white " ><i class="fa fa-file"></i></a>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_created"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_issued"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_paid"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <?php if ($s["payment_code"]) { ?>
                                                                        <span class="label label-default">
                                                                            <?= $s["payment_code"] ?>
                                                                        </span>
                                                                    <?php } ?>
                                                                </td>

                                                                <td>
                                                                    <span class="label bg-teal">
                                                                        <?= $s["type"] ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="btn-group ">
                                                                        <?php if ($s["status"] == "pending") { ?>
                                                                            <a href="" class="btn btn-warning btn-xs btn-block" data-toggle="modal" data-target="#documents_<?= $s["id"] ?>_request"><i class="fa fa-circle"></i> Request</a>
                                                                        <?php } else { ?>
                                                                            <?php if ($user["type"] != "patient") { ?>
                                                                                <a href="<?= linkPublic("files/{$s["file"]}") ?>" class="btn btn-default btn-xs btn-block" target="__blank"><i class="fa fa-circle-o"></i> View</a>
                                                                                    <!--<a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#documents_<?= $s["id"] ?>"><i class="fa fa-circle-o"></i> View</a>-->
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <?php if (in_array($user["type"], ["medtech", "admin"])) { ?>
                                                                            <a href="<?= linkTo("documents/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                                        <?php } ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div><!-- /.box -->


                                        </div><!-- /.box -->
                                    </div><!-- /.box -->
                                </div><!-- /.box --> 
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span class="">Prescriptions</span>

                                            </div><!-- /.box -->
                                            <div class="" style="overflow: auto">

                                                <table class="dtable table table-condensed table-bordered">
                                                    <thead>
                                                        <tr class="">
                                                            <th width="5">Date Created</th>
                                                            <th>Doctor</th>
                                                            <th>Remarks</th>

                                                            <th width="5">
                                                                <?php if (in_array($user["type"], ["doctor"])) { ?>
                                                                    <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addprescriptions">
                                                                        <i class="fa fa-plus-circle"></i> Add New prescription
                                                                    </button>
                                                                <?php } ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($prescriptions as $s) { ?>
                                                            <tr>

                                                                <td>
                                                                    <span class="label label-default">
                                                                        <?= $s["date_created"] ?>
                                                                    </span>
                                                                </td>
                                                                <td><?= $s["id_doctor"] ?></td>
                                                                <td><?= $s["remarks"] ?></td>
                                                                <td>
                                                                    <?php if ($user["type"] != "patient") { ?>
                                                                        <span class="btn-group ">
                                                                                <!--<a href="" class="btn btn-warning btn-xs btn-block" data-toggle="modal" data-target="#prescriptions_<?= $s["id"] ?>_request"><i class="fa fa-circle"></i> Request</a>-->
                                                                            <a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#prescriptions_<?= $s["id"] ?>_request"><i class="fa fa-circle-o"></i> View</a>
                                                                            <a href="<?= linkTo("prescriptions/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                                        </span>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>


                                            </div><!-- /.box -->


                                        </div><!-- /.box -->
                                    </div><!-- /.box -->
                                </div><!-- /.box --> 










                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div><!-- /.box -->







                </div><!-- /.box -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<?php foreach ($patient_demographics as $p) {
    ?>
    <div class="modal fade" id="demographics_<?= $p["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= $p["date_created"] ?> Patient Demographic Record</h4>
                </div>
                <form action="<?= linkTo("patients/" . $patient["id"] . "/demographics/add") ?>" method="POST">
                    <div class="modal-body">
                        <div class="row hidden">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Companion Name</label>
                                    <input type="text" name="companion_name" class="form-control" id="companion_name" readonly="" value="<?= $p["companion_name"] ?>"  placeholder="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Companion Complaint</label>
                                    <input type="text" name="companion_complaint" class="form-control" id="companion_complaint" readonly="" value="<?= $p["companion_complaint"] ?>" placeholder="">
                                </div>
                            </div>

                            <hr/>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Weight</label>
                                    <input type="text" name="weight" class="form-control" id="weight" readonly="" value="<?= $p["weight"] ?>" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Height</label>
                                    <input type="text" name="height" class="form-control" id="height" readonly="" value="<?= $p["height"] ?>" placeholder="">
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pulse</label>
                                    <input type="text" name="pulse" class="form-control" id="pulse" readonly="" value="<?= $p["pulse"] ?>" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Respiration</label>
                                    <input type="text" name="respiration" class="form-control" id="respiration" readonly="" value="<?= $p["respiration"] ?>" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Systolic</label>
                                    <input type="text" name="systolic" class="form-control" id="sysctolic" readonly="" value="<?= $p["systolic"] ?>" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Diastolic</label>
                                    <input type="text" name="diastolic" class="form-control" id="diastolic" readonly="" value="<?= $p["diastolic"] ?>" placeholder="">
                                </div>

                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Temperature</label>
                                    <input type="text" name="temperature" class="form-control" id="temperature" readonly="" value="<?= $p["temperature"] ?>" placeholder="">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!--                    <button type="reset" class="btn btn-warning">Clear Fields</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>-->
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } ?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Patient Demographic Record</h4>
            </div>
            <form action="<?= linkTo("patients/" . $patient["id"] . "/demographics/add") ?>" method="POST">
                <div class="modal-body">
                    <div class="row hidden">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Companion Name</label>
                                <input type="text" name="companion_name" class="form-control" id="companion_name" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Companion Complaint</label>
                                <input type="text" name="companion_complaint" class="form-control" id="companion_complaint" placeholder="">
                            </div>
                        </div>

                    </div>
                    <hr/>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Weight</label>
                                <input type="text" name="weight" class="form-control" id="weight" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Height</label>
                                <input type="text" name="height" class="form-control" id="height" placeholder="">
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pulse</label>
                                <input type="text" name="pulse" class="form-control" id="pulse" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Respiration</label>
                                <input type="text" name="respiration" class="form-control" id="respiration" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Systolic</label>
                                <input type="text" name="systolic" class="form-control" id="sysctolic" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Diastolic</label>
                                <input type="text" name="diastolic" class="form-control" id="diastolic" placeholder="">
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Temperature</label>
                                <input type="text" name="temperature" class="form-control" id="temperature" placeholder="">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="reset" class="btn btn-warning">Clear Fields</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="addSchedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--<div class="modal-dialog">-->
    <div class="row cmodal">
        <div class="col-xs-12" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Schedule</h4>
                </div>
                <form action="<?= linkTo("schedules/add/" . $id) ?>" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Doctor</label>
                                    <select class="form-control" name="id_doctor">
                                        <?php foreach ($users as $t) { ?>
                                            <?php if ($t["type"] == "doctor") { ?>
                                                <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>                              
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nurse</label>
                                    <select class="form-control" name="id_nurse">
                                        <?php foreach ($users as $t) { ?>
                                            <?php if ($t["type"] == "nurse") { ?>
                                                <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>                                      
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Room</label>
                                    <select class="form-control" name="id_room">
                                        <?php foreach ([101, 102, 103, 104, 105, 106] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Bed</label>
                                    <select class="form-control" name="id_bed">
                                        <?php foreach ([1, 2, 3, 4, 5, 6] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                              
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Triange</label>
                                    <select class="form-control" name="triange">
                                        <?php foreach ([1] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Type</label>
                                    <select class="form-control" name="type">
                                        <?php
                                        $rounds_type = ["consultation", "examination"];
                                        if (getUser("type") == "nurse") {
                                            $rounds_type = ["null"];
                                        }
                                        ?>
                                        <?php foreach ($rounds_type as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Status</label>
                                    <select class="form-control" name="status">
                                        <?php
                                        $statuses = [];
                                        if ($s["status"] == "pending") {
                                            $statuses = ["confirmed", "rescheduled", "declined"];
                                        } else if ($s["status"] == "confirmed") {
                                            $statuses = ["accepted", "declined"];
                                        } else {
                                            $statuses = ["confirmed", "rescheduled", "declined", "accepted"];
                                        }
                                        if (getUser("type") == "nurse") {
                                            $statuses = ["null"];
                                        }
                                        ?>
                                        <?php foreach ($statuses as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date Start</label>
                                    <input type="datetime" name="date_start" class="form-control" id="date_start" value="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date End</label>
                                    <input type="datetime" name="date_end" class="form-control" id="date_end" value="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Remarks</label>
                                    <textarea class="form-control" name="remarks" rows="4" cols="20"></textarea>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                                                                            <!--<a href="<?= linkTo("schedules/delete/" . $s["id"]) ?>" class="btn btn-success "><i class="fa fa-plus-circle"></i> Accept</a>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addScheduleSelf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--<div class="modal-dialog">-->
    <div class="row cmodal">
        <div class="col-xs-12" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Schedule</h4>
                </div>
                <form action="<?= linkTo("schedules/add/" . $id . "/self") ?>" method="POST">
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Type</label>
                                    <select class="form-control" name="type">
                                        <?php
                                        $rounds_type = ["consultation", "examination"];
                                        if (getUser("type") == "nurse") {
                                            $rounds_type = ["null"];
                                        }
                                        ?>
                                        <?php foreach ($rounds_type as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date Start</label>
                                    <input type="datetime" name="date_start" class="form-control" id="date_start" value="">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Remarks</label>
                                    <textarea class="form-control" name="remarks" rows="4" cols="20"></textarea>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                                                                            <!--<a href="<?= linkTo("schedules/delete/" . $s["id"]) ?>" class="btn btn-success "><i class="fa fa-plus-circle"></i> Accept</a>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





<?php foreach ($documents as $s) { ?>
    <div class="modal fade" id="documents_<?= $s["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-xs-12" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Schedule #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("documents/edit/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Patient</label>
                                    <select class="form-control" name="id_patient">
                                        <option value="<?= $s["id_patient"] ?>" ><?= $s["id_patient"] ?></option>
                                        <?php foreach ([0, 1, 2, 3, 4, 5, 6, 7, 8, 9] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="<?= $s["status"] ?>" ><?= $s["status"] ?></option>
                                        <?php foreach (["paid", "pending", "released"] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Original File</label>
                                        <input type="text" readonly="" class="form-control" value="<?= $s["file"] ?>" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type</label>
                                        <select class="form-control" name="type">
                                            <option value="<?= $s["type"] ?>" ><?= $s["type"] ?></option>
                                            <?php foreach (["x-ray", "form", "QR", "lambda"] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" name="name" class="form-control" value="<?= $s["name"] ?>" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">File</label>
                                        <input type="file" name="file" class="form-control" value="<?= $s["file"] ?>">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Issued</label>
                                        <input type="datetime" name="date_issued" class="form-control" value="<?= $s["date_issued"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Paid</label>
                                        <input type="datetime" name="date_paid" class="form-control" value="<?= $s["date_paid"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Payment Code</label>
                                        <input type="text" name="payment_code" class="form-control" value="<?= $s["payment_code"] ?>">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php foreach ($documents as $s) { ?>
    <div class="modal fade" id="documents_<?= $s["id"] ?>_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-md-6 col-md-offset-3" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Schedule #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("documents/request/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Issued</label>
                                        <input type="datetime" name="date_issued" readonly="" class="form-control" value="<?= $s["date_issued"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="datetime" name="name" readonly="" class="form-control" value="<?= $s["name"] ?>"  placeholder="">
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Payment Code</label>
                                        <input type="text" name="payment_code" class="form-control" value="">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>




<div class="modal fade" id="addDocuments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Document</h4>
            </div>

            <form action="<?= linkTo("documents/add") ?>" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Patient</label>
                            <select class="form-control" name="id_patient">
                                <option value="<?= $id ?>" ><?= $id ?></option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" name="status">
                                <?php foreach (["paid", "pending", "released"] as $t) { ?>
                                    <option value="<?= $t ?>" ><?= $t ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type</label>
                                <select class="form-control" name="type">
                                    <?php foreach (["x-ray", "form", "QR", "lambda"] as $t) { ?>
                                        <option value="<?= $t ?>" ><?= $t ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" name="name" class="form-control"placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">File</label>
                                <input type="file" name="file" class="form-control">
                                <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Date Issued</label>
                                <input type="datetime" name="date_issued" class="form-control"  placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date Paid</label>
                                <input type="datetime" name="date_paid" class="form-control"  placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Payment Code</label>
                                <input type="text" name="payment_code" class="form-control">
                                <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php foreach ($prescriptions as $s) { ?>
    <div class="modal fade" id="prescriptions_<?= $s["id"] ?>_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-md-10 col-md-offset-1" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">prescription #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("prescriptions/request/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Patient</label>
                                    <select class="form-control" name="id_patient">

                                        <option value="<?= $id ?>" ><?= $patient["last_name"] . ", " . $patient["first_name"] ?></option>

                                    </select>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Remarks</label>
                                        <textarea readonly="" name="remarks" class="form-control" rows="4" cols="20"><?= $s["remarks"] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-8">

                                    <table class="table table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th width="20%">Medication</th>
                                                <th width="20%">Frequency</th>
                                                <th>Dose</th>
                                                <th>Route</th>

                                            </tr>
                                        </thead>
                                        <tbody id="structure_table">
                                            <?php foreach ($medications as $m) { ?>
                                                <?php if ($m["id_patient_prescription"] == $s["id"]) { ?>
                                                    <tr>
                                                        <td>
                                                            <?= $m["medication"] ?>
                                                        </td>
                                                        <td>
                                                            <?= $m["frequency"] ?>
                                                        </td>
                                                        <td>
                                                            <?= $m["dose"] ?>
                                                        </td>
                                                        <td>
                                                            <?= $m["route"] ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>



                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                                                                                                            <!--                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                                                                                                                                        <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>-->
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<div class="modal fade" id="addprescriptions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="row cmodal">
        <div class="col-md-10 col-md-offset-1">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New prescription</h4>
                </div>

                <form action="<?= linkTo("prescriptions/add") ?>" enctype="multipart/form-data" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Patient</label>
                                <select class="form-control" name="id_patient">

                                    <option value="<?= $id ?>" ><?= $patient["last_name"] . ", " . $patient["first_name"] ?></option>

                                </select>
                                <!--                                <label for="exampleInputEmail1">Status</label>
                                                                <select class="form-control" name="status">
                                <?php foreach (["paid", "pending", "released"] as $t) { ?>
                                                                                                                                                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                <?php } ?>
                                                                </select>-->
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Remarks</label>
                                    <textarea name="remarks" class="form-control" rows="4" cols="20"></textarea>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="20%">Medication</th>
                                            <th width="20%">Frequency</th>
                                            <th>Dose</th>
                                            <th>Route</th>
                                            <th width="5">
                                                <button type="button" id="prescription_add" class=" btn btn-success btn-xs"><i class="ion ion-plus"></i></button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="prescription_table">

                                    </tbody>
                                </table>


                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <table border="1">
        <thead>

        </thead>

        <tbody id="prescription_body">
            <tr>

                <td>
                    <input  type="text" name="medication[]" class="form-control" id="address" placeholder="" >
                </td>
                <td>
                    <input  type="text" name="frequency[]" class="form-control" id="cause" placeholder="" >
                </td>
                <td>
                    <input  type="text" name="dose[]" class="form-control" id="address" placeholder="" >
                </td>
                <td>
                    <input  type="text" name="route[]" class="form-control" id="cause" placeholder="" >
                </td>

                <td>
                    <button type="button" class="remove btn btn-danger btn-xs"><i class="ion ion-trash-a"></i></button>
                </td>
            </tr>
        </tbody>
    </table>

</div>
<?php foreach ($schedules as $s) { ?>
    <?php
    $doctor = [];
    $nurse = [];
    foreach ($users as $u) {
        if ($s["id_doctor"] == $u["id"]) {
            $doctor = $u;
        }
        if ($s["id_nurse"] == $u["id"]) {
            $nurse = $u;
        }
    }
    ?>
    <div class="modal fade" id="schedules_<?= $s["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-xs-12" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Schedule #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("schedules/edit/" . $s["id"]) ?>" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Doctor</label>
                                        <select class="form-control" name="id_doctor">
                                            <option value="<?= $s["id_doctor"] ?>" ><?= $doctor["last_name"] ?>, <?= $doctor["first_name"] ?></option>
                                            <?php foreach ($users as $t) { ?>
                                                <?php if ($t["type"] == "doctor") { ?>
                                                    <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>                              

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nurse</label>
                                        <select class="form-control" name="id_nurse">

                                            <option value="<?= $s["id_nurse"] ?>" ><?= $nurse["last_name"] ?>, <?= $nurse["first_name"] ?></option>
                                            <?php foreach ($users as $t) { ?>
                                                <?php if ($t["type"] == "nurse") { ?>
                                                    <option value="<?= $t["id"] ?>" ><?= $t["last_name"] ?>, <?= $t["first_name"] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>                                      
                                </div>

                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Room</label>
                                        <select class="form-control" name="id_room">
                                            <option value="<?= $s["id_room"] ?>" ><?= $s["id_room"] ?></option>
                                            <?php foreach ([101, 102, 103, 104, 105, 106] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Bed</label>
                                        <select class="form-control" name="id_bed">
                                            <option value="<?= $s["id_bed"] ?>" ><?= $s["id_bed"] ?></option>
                                            <?php foreach ([1, 2, 3, 4, 5, 6] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                              
                                </div>

                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Triange</label>
                                        <select class="form-control" name="triange">
                                            <?php foreach ([1] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type</label>
                                        <select class="form-control" name="type">
                                            <?php
                                            $rounds_type = ["consultation", "examination"];
                                            ?>
                                            <?php foreach ($rounds_type as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Status</label>
                                        <select class="form-control" name="status">

                                            <?php
                                            $statuses = [];
                                            if ($s["status"] == "pending") {
                                                $statuses = ["confirmed", "rescheduled", "declined", "null"];
                                            } else if ($s["status"] == "confirmed") {
                                                $statuses = ["accepted", "declined", "null"];
                                            }
                                            ?>
                                            <?php foreach ($statuses as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Start</label>
                                        <input type="datetime" name="date_start" class="form-control" id="date_start" value="<?= $s["date_start"] ?>">
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="exampleInputEmail1">Date End</label>
                                        <input type="datetime" name="date_end" class="form-control" id="date_end" value="<?= $s["date_end"] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Remarks</label>
                                        <textarea class="form-control" name="remarks" rows="4" cols="20"><?= $s["remarks"] ?></textarea>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="modal-footer">

                                                                                                                                                                                <!--<a href="<?= linkTo("schedules/delete/" . $s["id"]) ?>" class="btn btn-success "><i class="fa fa-plus-circle"></i> Accept</a>-->
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>









<script>
    $(".dtable").dataTable();
    $('[type=datetime]').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });




    $("body").on("click", ".remove", function () {
        $(this).parent().parent().remove();
    });

    $("body").on("click", "#prescription_add", function () {
        $("#prescription_table").append($("#prescription_body").html());
    });


</script>

<?php
include linkPage("template/footer_admin");
?>