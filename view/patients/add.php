<?php
include linkPage("template/header_admin");
$user = fromSession("user");
$last_id = getTable("users", ["ORDER" => "id DESC"]);
$last_id = 1 + (int)$last_id["id"];
while (strlen($last_id) < 4) {
    $last_id = "0" . $last_id;
}
//var_dump($last_id);
//die();
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("Add New Patient Record") ?>
                    <!--<small></small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Patients</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <form action="<?= linkTo("patients/add") ?>" method="POST">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Emergency?</label>
                                                <select name="" class="form-control " id="compo">
                                                    <?php foreach (["yes", "no"] as $a) { ?>
                                                        <option value="<?= $a ?>"><?= $a ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="comp">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Companion Name</label>
                                                    <input type="text" name="companion_name" class="form-control" id="companion_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Companion Complaint</label>
                                                    <input type="text" name="companion_complaint" class="form-control" id="companion_complaint" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="picture">Picture</label>
                                                <div class="well" style="min-height: 10em; max-height: 100%; padding: 1em " >
                                                    <img class="img-thumbnail preview" src="<?= linkPublic("favicon.png") ?>" height="100%" style="width : 100%; margin: auto" >
                                                </div>
                                                <input id="picture" type="file" name="picture" class="form-control" id="picture" placeholder="picture">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">First Name</label>
                                                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Middle Name</label>
                                                        <input type="text" name="middle_name" class="form-control" id="middle_name" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Last Name</label>
                                                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input type="email" name="email" class="form-control" id="email" placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Contact Number</label>
                                                        <input type="text" name="contact_number" class="form-control" id="contact_number" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Address</label>
                                                        <textarea class="form-control" name="address" rows="5" cols="20"></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Patient Number</label>
                                                        <input type="name" name="patient_number" class="form-control" readonly="" value="" placeholder="SN0001"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Status</label>
                                                        <input type="name" class="form-control" readonly="" value="active"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Birthdate</label>
                                                <input type="date" name="birthday" class="form-control" id="birthday" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Gender</label>
                                                <select class="form-control" name="gender">
                                                    <?php foreach (["male", "female"] as $g) { ?>
                                                        <option><?= $g ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Blood Type</label>
                                                <select class="form-control" name="blood_type">
                                                    <?php foreach (["A+", "A-", "B+", "B-", "O", "AB+", "AB-"] as $g) { ?>
                                                        <option><?= $g ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row comp2">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nationality</label>
                                                <input type="text" name="nationality" class="form-control" id="nationality" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Occupation</label>
                                                <input type="text" name="occupation" class="form-control" id="occupation" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Religion</label>
                                                <input type="text" name="religion" class="form-control" id="religion" placeholder="">
                                            </div>
                                        </div>
                                    </div>


                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Weight</label>
                                                <input type="text" name="weight" class="form-control" id="weight" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Height</label>
                                                <input type="text" name="height" class="form-control" id="height" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Pulse</label>
                                                <input type="text" name="pulse" class="form-control" id="pulse" placeholder="">
                                            </div>
                                            <div class="form-group hidden">
                                                <label for="exampleInputEmail1">Respiration</label>
                                                <input type="text" name="respiration" class="form-control" id="respiration" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group hidden">
                                                <label for="exampleInputEmail1">Systolic</label>
                                                <input type="text" name="systolic" class="form-control" id="sysctolic" placeholder="">
                                            </div>
                                            <div class="form-group hidden">
                                                <label for="exampleInputEmail1">Diastolic</label>
                                                <input type="text" name="diastolic" class="form-control" id="diastolic" placeholder="">
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Temperature</label>
                                                <input type="text" name="temperature" class="form-control" id="temperature" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="clearfix ">
                                        <div class="btn-group pull-right ">
                                            <button type="reset" class=" btn btn-default"><i class="fa fa-eraser"></i> Clear Fields</button>
                                            <button type="submit" class=" btn btn-primary"><i class="fa fa-check-circle"></i> Submit New Patient Record</button>
                                        </div>
                                    </div>

                                </form>
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->
</body>

<script>
    $(".comp").hide();
    $("table").dataTable();


    if ($("#compo").val() == "yes") {
        $(".comp").show();
        $(".comp2").hide();
    } else {
        $(".comp").hide();
        $(".comp2").show();
    }
    $("#compo").on("click", function () {
        if ($(this).val() == "yes") {
            $(".comp").show();
            $(".comp2").hide();
        } else {
            $(".comp").hide();
            $(".comp2").show();
        }
    });


//    $("input [aria-controls='DataTables_Table_0']").val();

</script>
<script>
    $("table").dataTable();
    




    function readURL(input, fu) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(".preview").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    var aid = "", bid = "<?= $last_id?>";
    $("input[name='patient_number']").val("XX"+bid);
    $("body").on("change", "input[name='picture']", function () {
        readURL(this, $(this));
    });

    $("input[name='last_name']").change(function () {
        aid = $(this).val().substring(0, 2).toUpperCase();
        $("input[name='patient_number']").val(aid+bid);
    });


</script>

<?php
include linkPage("template/footer_admin");
?>