<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("patients") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Patients</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div role="tabpanel">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-pills nav-tabs nav-justified" role="tablist">
                                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Current Records</a></li>
                                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Non-current Records</a></li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <br/>
                                                <div role="tabpanel" class="tab-pane active mar20" id="home">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Patient Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("patients/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Patient</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($patients as $p) { ?>
                                                                <tr>
                                                                    <!--<td><?= $p["id"] ?></td>-->
                                                                    <td><?= $p["last_name"] . ", " . $p["first_name"] . " " . $p["middle_name"] ?></td>
                                                                    <td><?= $p["gender"] ?></td>
                                                                    <td><?= $p["birthday"] ?></td>
                                                                    <td><?= $p["contact_number"] ?></td>
                                                                    <td><?= $p["email"] ?></td>
                                                                    <td>

                                                                        <span class="btn-group btn-group-justified" style="width: 12em">
                                                                            <a href="<?= linkTo("patients/" . $p['id']) ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                            <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                                <a href="<?= linkTo("patients/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                            <?php } ?>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="profile">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Patient Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("patients/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Patient</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($patients as $p) { ?>
                                                                <tr>
                                                                    <!--<td><?= $p["id"] ?></td>-->
                                                                    <td><?= $p["last_name"] . ", " . $p["first_name"] . " " . $p["middle_name"] ?></td>
                                                                    <td><?= $p["gender"] ?></td>
                                                                    <td><?= $p["birthday"] ?></td>
                                                                    <td><?= $p["contact_number"] ?></td>
                                                                    <td><?= $p["email"] ?></td>
                                                                    <td>

                                                                        <span class="btn-group btn-group-justified" style="width: 6em">
                                                                            <a href="<?= linkTo("patients/" . $p['id']) ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                            <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                                <a href="<?= linkTo("patients/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                            <?php } ?>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>


                                    </div><!-- /.box -->
                                </div><!-- /.box -->
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div> 



            </section>
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<script>
    $("table").dataTable();

//    $("input [aria-controls='DataTables_Table_0']").val();

</script>

<?php
include linkPage("template/footer_admin");
?>