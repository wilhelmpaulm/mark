<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("prescriptionS") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> prescriptions</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="dtable table table-condensed table-bordered">
                                            <thead>
                                                <tr class="">
                                                    <th>Date Created</th>
                                                    <!--<th>Status</th>-->
                                                    <th>Patient</th>
                                                    <th>Doctor</th>
                                                    <th>Remarks</th>

                                                    <th width="5">
                                                        <!-- Button trigger modal -->
<!--                                                        <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addprescriptions">
                                                            <i class="fa fa-plus-circle"></i> Add New prescription
                                                        </button>-->
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($prescriptions as $s) { ?>
                                                    <tr>
<!--                                                        <td>
                                                            <span class="label label-info">
                                                                <?= $s["status"] ?>
                                                            </span>
                                                        </td>-->
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_created"] ?>
                                                            </span>
                                                        </td>
                                                        <td><?= $s["id_patient"] ?></td>
                                                        <td><?= $s["id_doctor"] ?></td>
                                                        <td><?= $s["remarks"] ?></td>



                                                        <td>
                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                    <!--<a href="" class="btn btn-warning btn-xs btn-block" data-toggle="modal" data-target="#prescriptions_<?= $s["id"] ?>_request"><i class="fa fa-circle"></i> Request</a>-->
                                                                <a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#prescriptions_<?= $s["id"] ?>_request"><i class="fa fa-circle-o"></i> View</a>
                                                                <a href="<?= linkTo("prescriptions/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>


                                    </div><!-- /.box -->
                                </div><!-- /.box --> 
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>


<?php foreach ($prescriptions as $s) { ?>
    <div class="modal fade" id="prescriptions_<?= $s["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-xs-12" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">prescription #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("prescriptions/edit/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Patient</label>
                                    <select class="form-control" name="id_patient">
                                        <option value="<?= $s["id_patient"] ?>" ><?= $s["id_patient"] ?></option>
                                        <?php foreach ($patients as $t) { ?>
                                            <option value="<?= $t["id"] ?>" ><?= $t["last_name"] . ", " . $t["first_name"] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="<?= $s["status"] ?>" ><?= $s["status"] ?></option>
                                        <?php foreach (["paid", "pending", "released"] as $t) { ?>
                                            <option value="<?= $t ?>" ><?= $t ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Original File</label>
                                        <input type="text" readonly="" class="form-control" value="<?= $s["file"] ?>" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type</label>
                                        <select class="form-control" name="type">
                                            <option value="<?= $s["type"] ?>" ><?= $s["type"] ?></option>
                                            <?php foreach (["x-ray", "form", "QR", "lambda"] as $t) { ?>
                                                <option value="<?= $t ?>" ><?= $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" name="name" class="form-control" value="<?= $s["name"] ?>" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">File</label>
                                        <input type="file" name="file" class="form-control" value="<?= $s["file"] ?>">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Issued</label>
                                        <input type="datetime" name="date_issued" class="form-control" value="<?= $s["date_issued"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date Paid</label>
                                        <input type="datetime" name="date_paid" class="form-control" value="<?= $s["date_paid"] ?>"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Payment Code</label>
                                        <input type="text" name="payment_code" class="form-control" value="<?= $s["payment_code"] ?>">
                                        <!--<input type="text" name="payment_code" class="form-control" data-mask="999-999-99" >-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php foreach ($prescriptions as $s) { ?>
    <div class="modal fade" id="prescriptions_<?= $s["id"] ?>_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-md-10 col-md-offset-1" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">prescription #<?= $s["id"] ?></h4>
                    </div>
                    <form action="<?= linkTo("prescriptions/request/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Patient</label>
                                    <select class="form-control" name="id_patient">
                                      
                                            <?php foreach ($patients as $t) { ?>
                                            <?php if ($s["id_patient"] == $t["id"]) { ?>
                                                <option value="<?= $t["id"] ?>" ><?= $t["last_name"] . ", " . $t["first_name"] ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        
                                    </select>
<!--                                    <label for="exampleInputEmail1">Status</label>
                                    <select  class="form-control" name="status">
                                        <option value="" ><?= $s["status"] ?></option>
                                    </select>-->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Remarks</label>
                                        <textarea readonly="" name="remarks" class="form-control" rows="4" cols="20"><?= $s["remarks"] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-8">

                                    <table class="table table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th width="20%">Medication</th>
                                                <th width="20%">Frequency</th>
                                                <th>Dose</th>
                                                <th>Route</th>

                                            </tr>
                                        </thead>
                                        <tbody id="structure_table">
                                            <?php foreach ($medications as $m) { ?>
                                                <?php if ($m["id_patient_prescription"] == $s["id"]) { ?>
                                                    <tr>
                                                        <td>
                                                            <?= $m["medication"] ?>
                                                        </td>
                                                        <td>
                                                            <?= $m["frequency"] ?>
                                                        </td>
                                                        <td>
                                                            <?= $m["dose"] ?>
                                                        </td>
                                                        <td>
                                                            <?= $m["route"] ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>



                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

    <!--                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                                <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>-->
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<div class="modal fade" id="addprescriptions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="row cmodal">
        <div class="col-md-10 col-md-offset-1">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New prescription</h4>
                </div>

                <form action="<?= linkTo("prescriptions/add") ?>" enctype="multipart/form-data" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Patient</label>
                                 <select class="form-control" name="id_patient">
                                        <?php foreach ($patients as $t) { ?>
                                            <option value="<?= $t["id"] ?>" ><?= $t["last_name"].", ".$t["first_name"] ?></option>
                                        <?php } ?>
                                    </select>
<!--                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" name="status">
                                    <?php foreach (["paid", "pending", "released"] as $t) { ?>
                                        <option value="<?= $t ?>" ><?= $t ?></option>
                                    <?php } ?>
                                </select>-->
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Remarks</label>
                                    <textarea name="remarks" class="form-control" rows="4" cols="20"></textarea>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="20%">Medication</th>
                                            <th width="20%">Frequency</th>
                                            <th>Dose</th>
                                            <th>Route</th>
                                            <th width="5">
                                                <button type="button" id="structure_add" class=" btn btn-success btn-xs"><i class="ion ion-plus"></i></button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="structure_table">

                                    </tbody>
                                </table>


                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <table border="1">
        <thead>

        </thead>

        <tbody id="structure_body">
            <tr>

                <td>
                    <input  type="text" name="medication[]" class="form-control" id="address" placeholder="" >
                </td>
                <td>
                    <input  type="text" name="frequency[]" class="form-control" id="cause" placeholder="" >
                </td>
                <td>
                    <input  type="text" name="dose[]" class="form-control" id="address" placeholder="" >
                </td>
                <td>
                    <input  type="text" name="route[]" class="form-control" id="cause" placeholder="" >
                </td>

                <td>
                    <button type="button" class="remove btn btn-danger btn-xs"><i class="ion ion-trash-a"></i></button>
                </td>
            </tr>
        </tbody>
    </table>

</div>










<script>
    $(".dtable").dataTable();
    $('[type=datetime]').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });




    $("body").on("click", ".remove", function () {
        $(this).parent().parent().remove();
    });

    $("body").on("click", "#structure_add", function () {
        $("#structure_table").append($("#structure_body").html());
    });


</script>

<?php
include linkPage("template/footer_admin");
?>