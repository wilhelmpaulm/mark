<header class="main-header c-alizarin">
    <a href="<?= linkTo("")?>" class="logo c-alizarin">
<!--        <span class="pull-left image">
            <img src="../../../../public/favicon.png" height="35" class="img-circle" alt="User Image" />
        </span>-->
        P<i class="fa fa-clock-o"></i>ll</a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="https://graph.facebook.com/<?= $user["fb_id"]?>/picture" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= $user["username"]?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="" class="img-circle" alt="User Image" />
                            <?php if ($user != null) { ?>
                                <p>
                                    <?= $user["first_name"]." ".$user["last_name"] ?>
                                    <small><?= $user["email"] ?></small>
                                    <small><?= $user["gender"] ?></small>
                                </p>
                            <?php } else { ?>
                            <?php } ?>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body ">
                            <div class="col-xs-4 text-center">
                                <!--<a href="#">Reports</a>-->
                                    <a href="<?= linkTo("logout") ?>"><i class="ion ion-gear-b"></i> Settings</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <!--<a href="#">Profile</a>-->
                            </div>
                            <div class="col-xs-4 text-center">
                                <?php if ($user != null) { ?>
                                    <a href="<?= linkTo("logout") ?>"><i class="ion ion-log-out"></i> Logout</a>
                                <?php } else { ?>
                                    <a href="<?= linkTo("login") ?>"><i class="ion ion-log-in"></i> Login</a>
                                <?php } ?>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <!--                                <li class="user-footer">
                                                            <div class="pull-left">
                                                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                                                            </div>
                                                            <div class="pull-right">
                                                                <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                                            </div>
                                                        </li>-->
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>