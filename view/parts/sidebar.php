<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" >
        <!-- Sidebar user panel -->
        <?php if ($user != null) { ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="" class="img-circle" alt="User Image" />
                </div>

                <div class="pull-left info text-center">
                    <p><?= $user["username"] ?></p>
                    <a href="#"><?= $user["first_name"] . " " . $user["last_name"] ?></a>
                </div>
            </div>

        <?php } else { ?>

        <?php } ?>

        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" style="">
            <!--<span class="hidden">-->
            <?php if($user["type"] == "medtech"){?>
            <li class="header">MED TECH / LAB ASSISTANT</li>
            <li><a href="<?= linkTo("patients") ?>"><i class="fa fa-users"></i> Patients</a></li>
            <?php }?>
            <!--</span>-->
            <!--<span class="hidden">-->
            <?php if($user["type"] == "nurse"){?>
            <li class="header">NURSE / FELLOW</li>
            <li><a href="<?= linkTo("patients") ?>"><i class="fa fa-users"></i> Patients</a></li>
            <li><a href="<?= linkTo("rounds") ?>"><i class="ion ion-refresh"></i> Rounds</a></li>
            <?php }?>
            <!--<li><a href="<?= linkTo("history") ?>"><i class="fa fa-book"></i> History</a></li>-->
            <!--</span>-->
            <!--<span class="hidden">-->
            <?php if($user["type"] == "doctor"){?>
            <li class="header">DOCTOR</li>
            <li><a href="<?= linkTo("patients") ?>"><i class="fa fa-users"></i> Patients</a></li>
            <li><a href="<?= linkTo("rounds") ?>"><i class="ion ion-refresh"></i> Rounds</a></li>
            <li><a href="<?= linkTo("prescriptions") ?>"><i class="fa fa-stethoscope"></i> Prescriptions</a></li>
            <?php }?>
            <!--</span>-->
            <!--<span class="hidden">-->
<!--            <li class="header">PATIENT</li>
            <li><a href="<?= linkTo("requests") ?>"><i class="ion ion-archive"></i> Requests</a></li>
            <li><a href="<?= linkTo("documents") ?>"><i class="ion ion-folder"></i> Documents</a></li>-->
            <!--</span>-->
            <!--<span class="hidden">-->
            <?php if($user["type"] == "admin"){?>
            <li class="header">ADMIN</li>
            <li><a href="<?= linkTo("patients") ?>"><i class="fa fa-users"></i> Patients</a></li>
            <li><a href="<?= linkTo("employees") ?>"><i class="fa fa-user-md"></i> Employees</a></li>
            <!--<li><a href="<?= linkTo("requests") ?>"><i class="ion ion-archive"></i> Requests</a></li>-->
            <!--<li><a href="<?= linkTo("room_assignments") ?>"><i class="ion ion-home"></i> Room Assignment</a></li>-->
            <?php }?>
            <?php if($user["type"] == "patient"){?>
            <li class="header">PATIENT</li>
            <li><a href="<?= linkTo("patients/".fromSession("puser")["id"]) ?>"><i class="fa fa-users"></i> Me</a></li>
            <?php }?>
            <!--</span>-->
            <li class="header">GENERAL</li>
            <?php if($user["type"] != "medtech"){?>
            <li><a href="<?= linkTo("schedules") ?>"><i class="ion ion-calendar"></i> Schedules</a></li>
            <?php }?>
            <li><a href="<?= linkTo("forums") ?>"><i class="ion ion-chatboxes"></i> forums</a></li>
            <!--<li><a href="<?= linkTo("updates") ?>"><i class="ion ion-loop"></i> Updates</a></li>-->
            <li><a href="<?= linkTo("documents") ?>"><i class="ion ion-document-text"></i> Documents</a></li>

            <li class="header">HELP</li>
            <li><a href="../../documentation/index.html"><i class="fa fa-question-circle"></i> FAQs</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>