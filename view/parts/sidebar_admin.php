<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" >
        <!-- Sidebar user panel -->
        <?php if ($user != null) { ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="" class="img-circle" alt="User Image" />
                </div>

                <div class="pull-left info text-center">
                    <p><?= $user["username"] ?></p>
                    <a href="#"><?= $user["first_name"] . " " . $user["last_name"] ?></a>
                </div>
            </div>

        <?php } else { ?>

        <?php } ?>

        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" style="">
            <li class="header">PATIENTS</li>
            <li><a href="<?= linkTo("patients") ?>"><i class="fa fa-info-circle"></i> Patients</a></li>

            <li class="header">HELP</li>
            <li><a href="../../documentation/index.html"><i class="fa fa-question-circle"></i> FAQs</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>