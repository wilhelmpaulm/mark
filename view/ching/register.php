<?php
include linkPage("template/header");
?>
<body class="row">
    <br/>
    <br/>
    <div class="col-md-4 col-md-offset-4">
        <div class="register-box-body">
            <div class="text-center " >
                <a class="fa-5x c-alizarin" href="<?= linkTo("") ?>">
                    <img src="../../../../public/logo_dlsu.gif" height="85" class="img-circle" alt="User Image" />
                    <img src="../../../../public/logo_phc.jpg" height="85" class="img-rounded" alt="User Image" />
                    HIS:RMS
                </a>
            </div>
            <p class="login-box-msg">Register as a new user</p>
            <form action="<?= linkTo("register") ?>" method="post">
                <div class="row">
                    <div class="form-group has-feedback col-xs-6">
                        <input type="text"  name="first_name" class="form-control " placeholder="First name"/>
                    </div>
                    <div class="form-group has-feedback col-xs-6">
                        <input type="text"  name=last_name" class="form-control " placeholder="Last name"/>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="text" class="form-control" placeholder="Username"/>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="Email"/>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password"/>
                </div>
                <div class="row">
                    <div class="col-xs-8">    
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> I agree to the <a href="#">terms.</a>
                            </label>
                        </div>                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div><!-- /.col -->
                </div>
            </form>        

            <div class="social-auth-links text-center">
                <!--<p> OR </p>-->
                <hr>
                <span class="btn-group btn-group-justified">
                    <a href="<?= linkTo("register/fb")?>" class="btn btn-block btn-facebook btn-flat"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="btn btn-block btn-twitter btn-flat"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="btn btn-block btn-google-plus btn-flat"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="btn btn-block btn-tumblr btn-flat"><i class="fa fa-tumblr"></i></a>
                    <a href="#" class="btn btn-block btn-instagram btn-flat"><i class="fa fa-instagram"></i></a>
                    <!--<br>-->
                </span>
                <hr>
                <span class="btn-group btn-group-justified" style="width: 12em">
                    <a href="login.html" class="btn btn-block btn-warning btn-flat text-center">I'm already a user</a>
                </span>
                <!--<a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>-->
            </div>

        </div><!-- /.form-box -->
    </div><!-- /.register-box -->


    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>


</body>

<?php
include linkPage("template/footer_admin");
?>