<?php
include linkPage("template/header");
?>
<body class="row">
    <br/>
    <br/>
    <div class="col-md-4 col-md-offset-4">
        <div class="register-box-body">
            <div class="text-center " >
                <a class="fa-5x c-alizarin" href="<?= linkTo("") ?>">
                    <img src="../../../../public/logo_dlsu.gif" height="120" class="img-circle" alt="User Image" />
                    <img src="../../../../public/logo_phc.jpg" height="120" class="img-rounded" alt="User Image" />
                    HIS:RMS
                </a>
            </div>
            <hr>
            <form class="" action="<?= linkTo("login") ?>" method="post">
                <!--<p class="login-box-msg">Log in </p>-->

                <div class="form-group has-feedback">
                    <input type="text" name="email" class="form-control" placeholder="Email"/>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                </div>

                <div class="row">
                    <div class="col-xs-8">    
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> Save user session</a>
                            </label>
                        </div>                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-check-circle"></i> Submit</button>
                    </div><!-- /.col -->
                </div>
            </form>        

            <div class="hidden social-auth-links text-center">
                <!--<p> OR </p>-->
                <hr>
                <span class="btn-group btn-group-justified">
                    <a href="<?= linkTo("register/fb") ?>" class="btn btn-block btn-facebook btn-flat"><i class="fa fa-facebook"></i> Log in with Facebook</a>
<!--                    <a href="#" class="btn btn-block btn-twitter btn-flat"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="btn btn-block btn-google-plus btn-flat"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="btn btn-block btn-tumblr btn-flat"><i class="fa fa-tumblr"></i></a>
                    <a href="#" class="btn btn-block btn-instagram btn-flat"><i class="fa fa-instagram"></i></a>-->
                    <!--<br>-->
                </span>
                <!--<hr>-->

 <!--<a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>-->
            </div>

        </div><!-- /.form-box -->
    </div><!-- /.register-box -->


    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>


</body>

<?php
include linkPage("template/footer_admin");
?>