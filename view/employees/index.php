<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("Employees") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Employees</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">


                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-justified" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">All</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Doctors</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Nurses</a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Medtech</a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="admin" role="tab" data-toggle="tab">Admin</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <br/>
                                        <br/>
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Type</th>
                                                                <th>Employee Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("employees/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Employee</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($employees as $p) { ?>
                                                                <tr>
                                                                    <!--<td><?= $p["id"] ?></td>-->
                                                                    <td>
                                                                        <span class="label label-default">
                                                                            <?= $p["type"] ?>
                                                                        </span>
                                                                    </td>
                                                                    <td><?= $p["last_name"] . ", " . $p["first_name"] ?></td>
                                                                    <td>
                                                                        <span class="label label-default">
                                                                            <?= $p["gender"] ?>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="label label-default">
                                                                            <?= $p["birthday"] ?>
                                                                        </span>
                                                                    </td>
                                                                    <td><?= $p["contact_number"] ?></td>
                                                                    <td><?= $p["email"] ?></td>
                                                                    <td>
                                                                        <span class="btn-group btn-group-justified">
                                                                            <a data-toggle="modal" data-target="#employee_<?= $p['id'] ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                            <a href="<?= linkTo("employees/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div><!-- /.box -->
                                            </div><!-- /.box -->
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Type</th>
                                                                <th>Employee Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("employees/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Employee</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($employees as $p) { ?>
                                                                <?php if ($p["type"] == "doctor") { ?>
                                                                    <tr>
                                                                        <!--<td><?= $p["id"] ?></td>-->
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["type"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["last_name"] . ", " . $p["first_name"] ?></td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["gender"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["birthday"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["contact_number"] ?></td>
                                                                        <td><?= $p["email"] ?></td>
                                                                        <td>
                                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                                <a data-toggle="modal" data-target="#employee_<?= $p['id'] ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                                <a href="<?= linkTo("employees/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div><!-- /.box -->
                                            </div><!-- /.box -->
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="messages">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Type</th>
                                                                <th>Employee Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("employees/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Employee</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($employees as $p) { ?>
                                                                <?php if ($p["type"] == "nurse") { ?>
                                                                    <tr>
                                                                        <!--<td><?= $p["id"] ?></td>-->
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["type"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["last_name"] . ", " . $p["first_name"] ?></td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["gender"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["birthday"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["contact_number"] ?></td>
                                                                        <td><?= $p["email"] ?></td>
                                                                        <td>
                                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                                <a data-toggle="modal" data-target="#employee_<?= $p['id'] ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                                <a href="<?= linkTo("employees/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div><!-- /.box -->
                                            </div><!-- /.box -->
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="settings">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Type</th>
                                                                <th>Employee Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("employees/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Employee</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($employees as $p) { ?>
                                                                <?php if ($p["type"] == "medtech") { ?>
                                                                    <tr>
                                                                        <!--<td><?= $p["id"] ?></td>-->
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["type"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["last_name"] . ", " . $p["first_name"] ?></td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["gender"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["birthday"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["contact_number"] ?></td>
                                                                        <td><?= $p["email"] ?></td>
                                                                        <td>
                                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                                <a data-toggle="modal" data-target="#employee_<?= $p['id'] ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                                <a href="<?= linkTo("employees/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div><!-- /.box -->
                                            </div><!-- /.box -->
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="admin">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <table class="table table-bordered table-responsive table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <!--<th>ID</th>-->
                                                                <th>Type</th>
                                                                <th>Employee Name</th>
                                                                <th>Gender</th>
                                                                <th>Birthdate</th>
                                                                <th>Contact Number</th>
                                                                <th>Email</th>
                                                                <th width="5">
                                                                    <?php if (in_array($user["type"], ["admin"])) { ?>
                                                                        <a href="<?= linkTo("employees/add") ?>" class="btn btn-success btn-xs btn-block"><i class="fa fa-plus-circle"></i> Add New Employee</a>
                                                                    <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($employees as $p) { ?>
                                                                <?php if ($p["type"] == "admin") { ?>
                                                                    <tr>
                                                                        <!--<td><?= $p["id"] ?></td>-->
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["type"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["last_name"] . ", " . $p["first_name"] ?></td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["gender"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="label label-default">
                                                                                <?= $p["birthday"] ?>
                                                                            </span>
                                                                        </td>
                                                                        <td><?= $p["contact_number"] ?></td>
                                                                        <td><?= $p["email"] ?></td>
                                                                        <td>
                                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                                <a data-toggle="modal" data-target="#employee_<?= $p['id'] ?>" class="btn btn-default btn-xs btn-block"><i class="fa fa-circle-o"></i> View </a>
                                                                                <a href="<?= linkTo("employees/delete/" . $p['id']) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete </a>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div><!-- /.box -->
                                            </div><!-- /.box -->
                                        </div><!-- /.box -->
                                    </div>
                            </div>

                        </div>

                    </div><!-- /.box -->
                </div><!-- /.box -->
        </div>                
    </div>                
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.3
    </div>
    <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->

</body>

<?php foreach ($employees as $s) { ?>
    <div class="modal fade" id="employee_<?= $s["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--<div class="modal-dialog">-->
        <div class="row cmodal">
            <div class="col-md-6 col-md-offset-3" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"> #<?= $s["last_name"] . ", " . $s["first_name"] ?></h4>
                    </div>
                    <form action="<?= linkTo("employees/edit/" . $s["id"]) ?>" enctype="multipart/form-data" method="POST">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">First Name</label>
                                        <input type="text" name="first_name" class="form-control" value="<?= $s["first_name"] ?>" id="first_name" placeholder="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name</label>
                                        <input type="text" name="last_name" class="form-control" value="<?= $s["last_name"] ?>" id="last_name" placeholder="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Birthdate</label>
                                        <input type="date" name="birthday" class="form-control"value="<?= $s["birthday"] ?>" id="birthday" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Gender</label>
                                        <select class="form-control" name="gender">
                                            <option><?= $s["gender"] ?></option>
                                            <?php foreach (["male", "female"] as $g) { ?>
                                                <option><?= $g ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type</label>
                                        <select class="form-control" name="type">
                                            <option><?= $s["type"] ?></option>
                                            <?php foreach (["doctor", "nurse", "admin"] as $g) { ?>
                                                <option><?= $g ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <!--                                            <div class="form-group">
                                                                                    <label for="exampleInputEmail1">Address</label>
                                                                                    <textarea class="form-control" name="address" rows="5" cols="20"></textarea>
                                                                                </div>-->
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Contact Number</label>
                                        <input type="text" name="contact_number" class="form-control" value="<?= $s["contact_number"] ?>" id="contact_number" placeholder="">
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" name="email" class="form-control" value="<?= $s["email"] ?>" id="email" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="password" name="password" class="form-control" value="<?= $s["password"] ?>" id="password" placeholder="">
                                    </div>
                                </div>
                                <!--                                        <div class="col-md-4">
                                                                        </div>-->

                            </div>

                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    $("table").dataTable();

//    $("input [aria-controls='DataTables_Table_0']").val();

</script>

<?php
include linkPage("template/footer_admin");
?>