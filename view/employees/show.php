<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-green">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Patient <?= $patient["first_name"] . " " . $patient["middle_name"] . " " . $patient["last_name"] ?>
                    <!--<small></small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr class="">
                                                    <th>Details</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td><?= $patient["first_name"] . " " . $patient["middle_name"] . " " . $patient["last_name"] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Gender</td>
                                                    <td><?= $patient["gender"] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Contact Number</td>
                                                    <td><?= $patient["contact_number"] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address</td>
                                                    <td><?= $patient["email"] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Blood Type</td>
                                                    <td><?= $patient["blood_type"] ?></td>
                                                </tr>

                                            </tbody>
                                        </table>


                                    </div><!-- /.box -->
                                </div><!-- /.box -->

                                <br/>
                                <br/>
                                <br/>




                            </div><!-- /.box -->
                            <div class="col-xs-8"><!-- /.box -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr class="">
                                                    <th>Patient Demographics</th>
                                                    <th>Weight</th>
                                                    <th>Height</th>
                                                    <th>Temperature</th>
                                                    <th>Respiration</th>
                                                    <th width="5">
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#myModal">
                                                            <i class="fa fa-plus-circle"></i> Add Demographic Record
                                                        </button>

                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($patient_demographics as $p) { ?>
                                                    <tr>
                                                        <td><?= $p["date_created"] ?></td>
                                                        <td><?= $p["weight"] ?></td>
                                                        <td><?= $p["height"] ?></td>
                                                        <td><?= $p["temperature"] ?></td>
                                                        <td><?= $p["respiration"] ?></td>
                                                        <td>
                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                <a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#demographics_<?= $p["id"] ?>"><i class="fa fa-circle-o"></i> View</a>
                                                                <a href="<?= linkTo("patients/demographics/delete/" . $p["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>


                                    </div><!-- /.box -->
                                </div><!-- /.box --> 
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div><!-- /.box -->
                </div><!-- /.box -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<?php foreach ($patient_demographics as $p){
     ?>
<div class="modal fade" id="demographics_<?= $p["id"]?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= $p["date_created"]?> Patient Demographic Record</h4>
            </div>
            <form action="<?= linkTo("patients/" . $patient["id"] . "/demographics/add") ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Companion Name</label>
                                <input type="text" name="companion_name" class="form-control" id="companion_name" readonly="" value="<?= $p["companion_name"]?>"  placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Companion Complaint</label>
                                <input type="text" name="companion_complaint" class="form-control" id="companion_complaint" readonly="" value="<?= $p["companion_complaint"]?>" placeholder="">
                            </div>
                        </div>

                    </div>
                    <hr/>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Weight</label>
                                <input type="text" name="weight" class="form-control" id="weight" readonly="" value="<?= $p["weight"]?>" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Height</label>
                                <input type="text" name="height" class="form-control" id="height" readonly="" value="<?= $p["height"]?>" placeholder="">
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pulse</label>
                                <input type="text" name="pulse" class="form-control" id="pulse" readonly="" value="<?= $p["pulse"]?>" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Respiration</label>
                                <input type="text" name="respiration" class="form-control" id="respiration" readonly="" value="<?= $p["respiration"]?>" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Systolic</label>
                                <input type="text" name="systolic" class="form-control" id="sysctolic" readonly="" value="<?= $p["systolic"]?>" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Diastolic</label>
                                <input type="text" name="diastolic" class="form-control" id="diastolic" readonly="" value="<?= $p["diastolic"]?>" placeholder="">
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Temperature</label>
                                <input type="text" name="temperature" class="form-control" id="temperature" readonly="" value="<?= $p["temperature"]?>" placeholder="">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                    <button type="reset" class="btn btn-warning">Clear Fields</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>-->
                </div>
            </form>
        </div>
    </div>
</div>

<?php }?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Patient Demographic Record</h4>
            </div>
            <form action="<?= linkTo("patients/" . $patient["id"] . "/demographics/add") ?>" method="POST">
                <div class="modal-body">
                    <div class="row hidden">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Companion Name</label>
                                <input type="text" name="companion_name" class="form-control" id="companion_name" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Companion Complaint</label>
                                <input type="text" name="companion_complaint" class="form-control" id="companion_complaint" placeholder="">
                            </div>
                        </div>

                    </div>
                    <hr/>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Weight</label>
                                <input type="text" name="weight" class="form-control" id="weight" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Height</label>
                                <input type="text" name="height" class="form-control" id="height" placeholder="">
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pulse</label>
                                <input type="text" name="pulse" class="form-control" id="pulse" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Respiration</label>
                                <input type="text" name="respiration" class="form-control" id="respiration" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Systolic</label>
                                <input type="text" name="systolic" class="form-control" id="sysctolic" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Diastolic</label>
                                <input type="text" name="diastolic" class="form-control" id="diastolic" placeholder="">
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Temperature</label>
                                <input type="text" name="temperature" class="form-control" id="temperature" placeholder="">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="reset" class="btn btn-warning">Clear Fields</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(".dtable").dataTable();

//    $("input [aria-controls='DataTables_Table_0']").val();

</script>

<?php
include linkPage("template/footer_admin");
?>