<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("Add NEW EMPLOYEE RECORD") ?>
                    <!--<small></small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Employees</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <form action="<?= linkTo("employees/add") ?>" method="POST">

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="picture">Picture</label>
                                                <div class="well" style="min-height: 10em; max-height: 100%; padding: 1em " >
                                                    <img class="img-thumbnail preview" src="<?= linkPublic("favicon.png") ?>" height="100%" style="width : 100%; margin: auto" >
                                                </div>
                                                <input id="picture" type="file" name="picture" class="form-control" id="picture" placeholder="picture">
                                                <label for="resume">Resume</label>
                                                <input id="resume" type="file" name="resume" class="form-control" id="picture" placeholder="resume">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">First Name</label>
                                                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Middle Name</label>
                                                        <input type="text" name="middle_name" class="form-control" id="middle_name" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Last Name</label>
                                                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Birthdate</label>
                                                        <input type="date" name="birthday" class="form-control" id="birthday" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Gender</label>
                                                        <select class="form-control" name="gender">
                                                            <?php foreach (["male", "female"] as $g) { ?>
                                                                <option value="<?= $g ?>"><?= ucfirst($g) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Type</label>
                                                        <select class="form-control" name="type">
                                                            <?php foreach (["doctor", "nurse", "admin", "medtech"] as $g) { ?>
                                                                <option value="<?= $g ?>"><?= ucfirst($g) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <!--                                            <div class="form-group">
                                                                                                    <label for="exampleInputEmail1">Address</label>
                                                                                                    <textarea class="form-control" name="address" rows="5" cols="20"></textarea>
                                                                                                </div>-->
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Contact Number</label>
                                                        <input type="text" name="contact_number" class="form-control" id="contact_number" placeholder="">
                                                    </div>

                                                </div>
                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input type="email" name="email" class="form-control" id="email" placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Password</label>
                                                        <input type="password" name="password" class="form-control" id="password" placeholder="">
                                                    </div>
                                                </div>
                                                <!--                                        <div class="col-md-4">
                                                                                        </div>-->

                                            </div>
                                        </div>
                                    </div>



                                    <hr>
                                    <div class="clearfix ">
                                        <div class="btn-group pull-right ">
                                            <button type="reset" class=" btn btn-default"><i class="fa fa-eraser"></i> Clear Fields</button>
                                            <button type="submit" class=" btn btn-primary"><i class="fa fa-check-circle"></i> Submit New Employee</button>
                                        </div>
                                    </div>

                                </form>
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<script>
    $("table").dataTable();

//    $("input [aria-controls='DataTables_Table_0']").val();

</script>
<script>
    $("table").dataTable();
    $('[type=datet]').datetimepicker({
        format: 'YYYY-MM-DD'
    });
</script>
<?php
include linkPage("template/footer_admin");
?>