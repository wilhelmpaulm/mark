<?php
$dir = $_SERVER['DOCUMENT_ROOT'];
include $dir . '/view/template/header_admin.php';
?>
<body class="skin-black">
    <!-- Site wrapper -->
    <div class="wrapper">


        <?php
        include 'template/header.php';
        ?> 

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <?php
        include 'template/sidebar.php';
        ?> 

        <!-- =============================================== -->

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Add New Client
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box box-default">
                    <!--<div class="box-header">-->
                    <!--<h3 class="box-title">Add New Client</h3>-->
                    <!--</div> /.box-header -->
                    <!-- form start -->
                    <form role="form"  action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/admin/client-process" method="POST">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="001">Client Name</label>
                                            <input type="text" name="name" class="form-control" id="001" placeholder="">
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputPassword1">User Name</label>
                                            <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Password</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group ">
                                                <label for="exampleInputPassword1">Contact Number</label>
                                                <input type="text" name="contact" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Email</label>
                                                <input type="text" name="email" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Address</label>
                                                <input type="text" name="address" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputPassword1">Remarks</label>
                                            <textarea name="remarks" class="form-control" id="exampleInputPassword1" placeholder="Password" rows="8" cols="20"></textarea>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="clearfix">
                                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                    </div>
                                </div>
                            </div>


                        </div><!-- /.box-body -->
<!--
                        <div class="box-footer clearfix">
                        </div>-->
                    </form>
                </div><!-- /.box -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<?php
include $dir . '/view/template/footer_admin.php';
?>