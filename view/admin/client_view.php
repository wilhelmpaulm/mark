<?php
$dir = $_SERVER['DOCUMENT_ROOT'];
include $dir . '/view/template/header_admin.php';
?>
<body class="skin-black">
    <!-- Site wrapper -->
    <div class="wrapper">


        <?php
        include 'template/header.php';
        ?> 

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <?php
        include 'template/sidebar.php';
        ?> 

        <!-- =============================================== -->

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Client List
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box box-default">
                    
                </div><!-- /.box -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<?php
include $dir . '/view/template/footer_admin.php';
?>