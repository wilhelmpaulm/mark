<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-green">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    Blank page
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </section>

            <section class="content">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Title</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        Start creating your amazing application!
                    </div>
                    <div class="box-footer">
                        Footer
                    </div>                
                </div>
            </section>
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>

<?php
include linkPage("template/footer_admin");
?>