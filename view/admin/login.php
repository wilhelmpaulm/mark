<?php
$dir = $_SERVER['DOCUMENT_ROOT'];
include $dir . '/view/template/header_admin.php';
?>
<body class="login-page bg-white">
    <div class="login-box">
        <div class="login-logo">
            <img src="../../../../public/favicon.png" height="80" class="user-image" alt="User Image"/>
              
            <a href="<?= linkTo("")?>">DILG - ching </a>
        </div><!-- /.login-logo -->
<!--        <p class="text-center">Fire Risk Assessment and Monitoring and Response and Recovery System</p>-->
        <div class="login-box-body">
            <!--<p class="login-box-msg">Sign in to start your session</p>-->
            <p class="login-box-msg">Fire Risk Assessment and Monitoring and Response and Recovery</p>
            <form action="http://<?= $_SERVER["HTTP_HOST"] ?>/login" method="post">
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Email"/>
                    <span class=""></span>
                </div>
                <div class="form-group ">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                    <span class=""></span>
                </div>
                <div class="row">
                    <div class="col-xs-7 col-xs-offset-1">    
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> Remember Me
                            </label>
                        </div>                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"> Sign In</button>
                    </div><!-- /.col -->
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-offset-1">
                        <a href="#">I forgot my password</a><br>
                        <!--<a href="register.html" class="text-center">Register a new membership</a>-->
                    </div>
                </div>
            </form>

            <!--            <div class="social-auth-links text-center">
                            <p>- OR -</p>
                            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
                            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
                        </div> /.social-auth-links -->


        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->


</body>

<?php
include $dir . '/view/template/footer_admin.php';
?>