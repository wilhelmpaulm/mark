<?php
$dir = $_SERVER['DOCUMENT_ROOT'];
include $dir . '/view/template/header_admin.php';
?>
<body class="skin-black">
    <!-- Site wrapper -->
    <div class="wrapper">


        <?php
        include 'template/header.php';
        ?> 

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <?php
        include 'template/sidebar.php';
        ?> 

        <!-- =============================================== -->

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Client List
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <!--<th>ID</th>-->
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th  width="5"></th>
                                            <th  width="5"></th>
                                            <th  width="5"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($clients as $client) { ?>
                                            <tr>
                                                <!--<td><?= $client["id"] ?></td>-->
                                                <td><?= $client["name"] ?></td>
                                                <td><?= $client["username"] ?></td>
                                                <td><?= $client["contact"] ?></td>
                                                <td><?= $client["email"] ?></td>
                                                <td><?= $client["address"] ?></td>
                                                <td width="5"><a href="http://<?= $_SERVER['HTTP_HOST']; ?>/admin/client/delete/<?= $client["id"] ?>" class="btn btn-xs btn-danger"><i class="ion ion-trash-a"></a></td>
                                                <td width="5"><a class="btn btn-xs btn-default"><i class="ion ion-eye"></a></td>
                                                <td width="5"><a class="btn btn-xs btn-default"><i class="ion ion-map"></a></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div><!-- /.box -->
                </div><!-- /.box -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>
<script>
    $("table").dataTable();
    
</script>
<?php
include $dir . '/view/template/footer_admin.php';
?>