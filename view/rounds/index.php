<?php
include linkPage("template/header_admin");
$user = fromSession("user");
?>
<body class="skin-red">
    <div class="wrapper">
        <?php
        include linkPage("parts/header");
        include linkPage("parts/sidebar");
        ?> 
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <h1>
                    <?= strtoupper("ROUNDS") ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Schedules</a></li>
                    <!--<li class="active"><?= "" ?></li>-->
                    <!--<li><a href="#">Examples</a></li>-->
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr class="">
                                                    <th>Patient</th>
                                                    <th>Status</th>
                                                    <th>Date Set</th>
                                                    <th>Date Start</th>
                                                    <th>Date End</th>
                                                    <th>Doctor</th>
                                                    <th>Nurse</th>
                                                    <th>Room</th>
                                                    <th>Bed</th>
                                                    <th>Type</th>
                                                    <th>Triange</th>
                                                    <th >
                                                        <!-- Button trigger modal -->
                                                        <!--                                                        <button type="button" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addSchedules">
                                                                                                                    <i class="fa fa-plus-circle"></i> Add Appointment
                                                                                                                </button>-->
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($rounds as $s) { ?>
                                                    <?php
                                                    $doctor = [];
                                                    $nurse = [];
                                                    $patient = [];
                                                    foreach ($users as $u) {
                                                        if ($s["id_doctor"] == $u["id"]) {
                                                            $doctor = $u;
                                                        }
                                                        if ($s["id_patient"] == $u["id"]) {
                                                            $patient = $u;
                                                        }
                                                        if ($s["id_nurse"] == $u["id"]) {
                                                            $nurse = $u;
                                                        }
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?= $patient["last_name"] ?>, <?= $patient["first_name"] ?></td>

                                                        <td>
                                                            <span class="label label-info">
                                                                <?= $s["status"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_created"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_start"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default">
                                                                <?= $s["date_end"] ?>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <?php if ($doctor): ?>
                                                                <?= $doctor["last_name"] ?>, <?= $doctor["first_name"] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($nurse): ?>
                                                                <?= $nurse["last_name"] ?>, <?= $nurse["first_name"] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?= $s["id_room"] ?></td>
                                                        <td><?= $s["id_bed"] ?></td>
                                                        <td>
                                                            <span class="label bg-teal">
                                                                <?= $s["type"] ?>
                                                            </span>
                                                        </td>
                                                        <td><?= $s["triange"] ?></td>
                                                        <td>
                                                            <span class="btn-group btn-group-justified" style="width: 12em">
                                                                <?php
                                                                $von = false;

                                                                foreach ($vitals as $v) {

//                                                                    echo (substr(currentdatetime(), 0, 10) === substr($v["date_created"], 0, 10));
                                                                    if ($v["id_patient"] == $s["id_patient"]) {
                                                                        if (substr(currentdatetime(), 0, 10) == substr($v["date_created"], 0, 10)) {
//                                                                            echo "asdasdasdasdasd";
                                                                            $von = true;
                                                                        } else {
//                                                                            echo "asdasd";
                                                                        }
                                                                    }
                                                                }
                                                                ?>


                                                                <?php if ($von) { ?>
                                                                    <a href="" class="btn btn-success btn-xs btn-block" data-toggle="modal" data-target="#vitals_add_<?= $s["id_patient"] ?>"><i class="fa fa-circle"></i> Vitals Record</a>
                                                                <?php } else { ?>
                                                                    <a href="" class="btn btn-warning btn-xs btn-block" data-toggle="modal" data-target="#vitals_add_<?= $s["id_patient"] ?>"><i class="fa fa-plus-circle"></i> Vitals Pending</i> </a>
                                                                <?php } ?>
                                                            <!--<a href="" class="btn btn-default btn-xs btn-block" data-toggle="modal" data-target="#vitals_<?= $s["id_patient"] ?>"><i class="fa fa-circle-o"></i> View Patient Vitals</a>-->
                                                            <!--<a href="<?= linkTo("rounds/delete/" . $s["id"]) ?>" class="btn btn-danger btn-xs btn-block"><i class="fa fa-minus-circle"></i> Delete</a>-->
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>


                                    </div><!-- /.box -->
                                </div><!-- /.box --> 
                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div>                
                </div>                
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.3
            </div>
            <strong>Copyright &copy; 2015 ching. </strong> All rights reserved.
        </footer>
    </div><!-- ./wrapper -->

</body>


<?php foreach ($patients as $p) { ?>
    <div class="modal fade" id="vitals_add_<?= $p["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="row cmodal">
            <div class="col-md-10 col-md-offset-1">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Remarks For Patient Vitals</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-8">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="30%">Date Created</th>
                                            <th width="5%">Doctor</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($vitals as $v) { ?>
                                            <?php
                                            $doctor = [];
                                           
                                            foreach ($users as $u) {
                                                if ($v["id_doctor"] == $u["id"]) {
                                                    $doctor = $u;
                                                }
                                              
                                            }
                                            ?>
                                            <?php if ($p["id"] == $v["id_patient"]) { ?>
                                                <tr>
                                                    <td>
                                                        <span class="label label-default">
                                                            <?= $v["date_created"] ?>
                                                        </span>
                                                    </td>
                                                 
                                                     <td>
                                                          <span class="label label-default">
                                                            <?php if ($doctor): ?>
                                                                <?= $doctor["last_name"] ?>, <?= $doctor["first_name"] ?>
                                                            <?php endif; ?>
                                                          </span>
                                                        </td>
                                                    <td>
                                                        <?= $v["remarks"] ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>

                            <div class="col-md-4">
                                <form action="<?= linkTo("patients/" . $p["id"] . "/vitals/add") ?>" method="POST">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Remarks</label>
                                                    <textarea class="form-control" name="remarks" rows="4" cols="20"></textarea>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                                        <!--<button type="reset" class="btn btn-warning"><i class="fa fa-circle-o"></i> Clear Fields</button>-->
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>

                    </div>

                </div>
            </div>
        </div>
    </div>
<?php } ?>










<script>
    $("table").dataTable();
    $('[type=datetime]').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script>

<?php
include linkPage("template/footer_admin");
?>